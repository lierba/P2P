package base.web.controller;

import base.pojo.SystemDictionary;
import base.pojo.UserInfo;
import base.service.ISystemDictionaryService;
import base.service.IUserInfoService;
import common.RequireLogin;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Iterator;
import java.util.List;

/**
 * 个人资料控制器
 */
@Controller@Scope("prototype")
public class BasicInfoController
{
    @Autowired
    private IUserInfoService            iUserInfoService        ;
    @Autowired
    private ISystemDictionaryService    iSystemDictionaryService;


    @RequireLogin
    @RequestMapping("basicInfo.html")
    public String basicInfoIndex(Model model)
    {
        model.addAttribute("userInfo",iUserInfoService.getCurrent())               ;
        List<SystemDictionary> list = iSystemDictionaryService.allSystemDictionary()            ;
        Iterator<SystemDictionary> iterator =  list.iterator()                                  ;
        while(iterator.hasNext())
        {
            SystemDictionary systemDictionary   =   iterator.next()                             ;
            model.addAttribute(systemDictionary.getSn()+"s"                        ,
                 iSystemDictionaryService.allSystemDictionaryItemBySn(systemDictionary.getSn()));
        }
        return "userInfo";
    }
    @RequireLogin
    @RequestMapping("basicInfo_save.do")
    @ResponseBody
    public ServerResponse basicInfoSave(UserInfo userInfo)
    {
        return iUserInfoService.updateBasicInfo(userInfo);
    }
}
