package base.web.controller;

import base.query.CommonQuery;
import business.pojo.RechargeOffline;
import business.service.IPlatformBankService;
import business.service.IRechargeOfflineService;
import common.RequireLogin;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import util.SendEmailUtil;

/**
 * 线下充值
 * @author LiuZhi
 */
@Controller@Scope("prototype")
public class RechargeOfflineController
{
    @Autowired
    private IPlatformBankService    iPlatformBankService    ;
    @Autowired
    private IRechargeOfflineService iRechargeOfflineService ;

    @RequireLogin
    @RequestMapping("recharge.html")
    public String recharge(Model model)
    {
        model.addAttribute("banks",iPlatformBankService.listPlatformBank());
        System.out.println(model);
        return "recharge";
    }

    @RequireLogin
    @RequestMapping("/rechargeApply.do")
    @ResponseBody
    public ServerResponse rechargeApply(RechargeOffline rechargeOffline)
    {
        return iRechargeOfflineService.apply(rechargeOffline);
    }
    @RequireLogin
    @RequestMapping("/rechargeList.html")
    public String rechargeList(Model model,@ModelAttribute("qo")CommonQuery commonQuery)
    {
        model.addAttribute("res",iRechargeOfflineService.rechargeList(commonQuery));
        return "recharge_list";
    }
}
