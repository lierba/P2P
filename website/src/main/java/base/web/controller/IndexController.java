package base.web.controller;

import business.query.BidRequestQuery;
import business.service.IBidRequestService;
import common.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 网站首页
 * @author LiuZhi
 */
@Controller@Scope("prototype")
public class IndexController
{
    @Autowired
    IBidRequestService iBidRequestService;

    @RequestMapping("index.html")
    public String index(Model model)
    {
        BidRequestQuery bidRequestQuery = new BidRequestQuery()                              ;
        bidRequestQuery.setBidRequestStates(new int[]{
                Const.BIDREQUEST_STATE_BIDDING                                               ,
                Const.BIDREQUEST_STATE_COMPLETE_PAY_BACK                                     ,
                Const.BIDREQUEST_STATE_PAYING_BACK                        })                 ;
        model.addAttribute("res",iBidRequestService.listAll(bidRequestQuery))   ;
        return "main";
    }

    @RequestMapping("invest.html")
    public String investIndex( )
    {
        return "invest";
    }

    @RequestMapping("investList.do")
    public String investList(BidRequestQuery bidRequestQuery,Model model)
    {
        if(bidRequestQuery.getBidRequestState() == -1)
        {
            bidRequestQuery.setBidRequestStates(new int[]{
                    Const.BIDREQUEST_STATE_BIDDING                                          ,
                    Const.BIDREQUEST_STATE_COMPLETE_PAY_BACK                                ,
                    Const.BIDREQUEST_STATE_PAYING_BACK                        })            ;
        }
        model.addAttribute("res",iBidRequestService.listAll(bidRequestQuery))  ;
        return "invest_list";
    }
}
