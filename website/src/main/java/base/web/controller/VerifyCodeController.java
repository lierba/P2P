package base.web.controller;

/**
 * 验证码相关的控制器
 * @author LiuZhi
 */

import base.service.IEmailService;
import base.service.IVerifyCodeService;
import common.RequireLogin;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller@Scope("prototype")
public class VerifyCodeController
{
    @Autowired
    IVerifyCodeService  iVerifyCodeService  ;
    @Autowired
    IEmailService       iEmailService       ;

    @RequireLogin
    @RequestMapping("sendVerifyCode.do")
    @ResponseBody
    public ServerResponse sendVerifyCode(String phoneNumber)
    {
        try {
            return iVerifyCodeService.sendVerifyCode(phoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
            return ServerResponse.createByErrorMsg("发送短信错误:"+e.getMessage());
        }
    }

    @RequireLogin
    @RequestMapping("bindPhone.do")
    @ResponseBody
    public ServerResponse bindPhone(String phoneNumber,String verifyCode)
    {
        return iVerifyCodeService.bindPhone(phoneNumber,verifyCode);
    }

    @RequestMapping("bindEmail.do")
    public String bindEmail(String checkCode, Model model)
    {
        ServerResponse sr   =   iEmailService.bindEmail(checkCode);
        if(sr.getStatus()==0)
        {
            model.addAttribute("success",true);
        }else
            {
                model.addAttribute("success",false);
                model.addAttribute("msg",sr.getMsg());
            }
        return "checkMail_result";
    }

    @RequireLogin
    @RequestMapping("sendEmailCode.do")
    @ResponseBody
    public ServerResponse sendEmailCode(String email)
    {
        try {
            return iEmailService.sendEmail(email);
        } catch (Exception e) {
            e.printStackTrace();
            return ServerResponse.createByErrorMsg("发送邮箱错误:"+e.getMessage());
        }
    }
}
