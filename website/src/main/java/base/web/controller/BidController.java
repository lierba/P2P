package base.web.controller;

import business.service.IBidRequestService;
import common.RequireLogin;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

@Controller@Scope("prototype")
public class BidController
{
    @Autowired
    IBidRequestService iBidRequestService;

    @RequireLogin
    @RequestMapping("borrow_bid.do")
    @ResponseBody
    public ServerResponse bid(Long bidRequestId, BigDecimal amount)
    {
        try {
            return iBidRequestService.bid(bidRequestId,amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByError();
    }
}
