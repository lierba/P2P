package base.web.controller;

import base.pojo.RealAuth;
import base.pojo.UserInfo;
import base.service.IRealAuthService;
import base.service.IUserInfoService;
import common.RequireLogin;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import util.UploadUtil;

import javax.servlet.ServletContext;

/**
 * 实名认证的控制器
 */
@Controller@Scope("prototype")
public class RealAuthController
{
    @Autowired
    private IUserInfoService iUserInfoService;
    @Autowired
    private IRealAuthService iRealAuthService;
    @Autowired
    private ServletContext   servletContext  ;
    @RequireLogin
    @RequestMapping("realAuth.html")
    public String realAuthIndex(Model model)
    {
        UserInfo current = iUserInfoService.getCurrent();
        if(current.getHasRealAuth())
        {   //已经实名认证
            model.addAttribute("realAuth",iRealAuthService.getRealAuth(current.getRealAuthId()));
            System.out.println(model);
            model.addAttribute("auditing",false);
            return "realAuth_result";
        }
        if(current.getRealAuthId() != null)
        {   //已提交审核资料
            model.addAttribute("auditing",true);
            return "realAuth_result";
        }
        return "realAuth";
    }

    /**
     *身份证图片上传请求
     * @file 与前台上传的名字一致
     */
    @RequestMapping("realAuthUpload.do")
    @ResponseBody
    public String realAuthUpload(MultipartFile file)
    {
        String basePath = servletContext.getRealPath("/upload");
        String fileName = UploadUtil.upload(file,basePath);
        return "/upload/"+fileName;
    }
    @RequireLogin
    @RequestMapping("realAuthSave.do")
    @ResponseBody
    public ServerResponse realAuthSave(RealAuth realAuth)
    {
        try {
            return iRealAuthService.realApply(realAuth);
        } catch (Exception e) {
            e.printStackTrace();
            return ServerResponse.createByErrorMsg(e.getMessage());
        }
    }
}
