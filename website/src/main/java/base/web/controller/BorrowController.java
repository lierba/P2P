package base.web.controller;

import base.pojo.LoginInfo;
import base.pojo.UserInfo;
import base.service.IAccountService;
import base.service.IRealAuthService;
import base.service.IUserFileService;
import base.service.IUserInfoService;
import business.pojo.BidRequest;
import business.service.IBidRequestService;
import common.Const;
import common.RequireLogin;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import util.BitOperationUtil;

@Controller@Scope("prototype")
public class BorrowController
{
    @Autowired
    private IAccountService     iAccountService     ;
    @Autowired
    private IUserInfoService    iUserInfoService    ;
    @Autowired
    private IBidRequestService  iBidRequestService  ;
    @Autowired
    private IRealAuthService    iRealAuthService    ;
    @Autowired
    private IUserFileService    iUserFileService    ;

    @RequestMapping("borrow.htm")
    public String borrowIndex(Model model)
    {
        LoginInfo loginInfo = UserContext.getCurrentUser();
        if(loginInfo    ==  null)
        {
            return "redirect:borrow.html";
        }
        model.addAttribute("account",iAccountService.getCurrent())  ;
        model.addAttribute("userInfo",iUserInfoService.getCurrent());
        model.addAttribute("creditBorrowScore", Const.BASE_SCORE)   ;
        return "borrow";
    }
    @RequireLogin
    @RequestMapping("borrowInfo.html")
    public String borrowInfo(Model model)
    {

        if(iBidRequestService.canApplyBidRequest())
        {
            model.addAttribute("minBidRequestAmount"
                    ,Const.SMALLEST_BIDREQUEST_AMOUNT)  ;
            model.addAttribute("minBidAmount"
                    ,Const.SMALLEST_BID_AMOUNT)         ;
            model.addAttribute("account"
                    ,iAccountService.getCurrent())      ;
            return "borrow_apply"   ;
        }
        return "borrow_apply_result";
    }
    @RequireLogin
    @RequestMapping("borrowApply.html")
    public String borrowApply(BidRequest bidRequest)
    {
        iBidRequestService.apply(bidRequest);
        return "redirect:borrowInfo.html";
    }

    @RequestMapping("borrowDetails.html")
    public String borrowDetails(Long id,Model model)
    {
        BidRequest  bidRequest  =   iBidRequestService.getBidRequest(id)                                        ;
        if(bidRequest   !=  null)
        {
            UserInfo    userInfo    =   iUserInfoService.get(bidRequest.getCreateUser().getId())                ;
            model.addAttribute("realAuth",iRealAuthService.getRealAuth(userInfo.getRealAuthId()))  ;
            model.addAttribute("userFiles",iUserFileService.listFilesByAudit(userInfo.getId()))    ;
            model.addAttribute("bidRequest",bidRequest)                                            ;
            model.addAttribute("userInfo",userInfo)                                                ;
            if(UserContext.getCurrentUser()!=null  )
            {//如果是当前用户
                if(UserContext.getCurrentUser().getId().equals(userInfo.getId()))
                {
                    model.addAttribute("self",true)                                   ;

                }else
                    {
                        model.addAttribute("self",false)                              ;
                        model.addAttribute("account",iAccountService.getCurrent())                 ;
                    }
            }else
                {
                    model.addAttribute("self",false)                                  ;
                }
        }
        System.out.println(model);
        return "borrow_info";
    }
}
