package base.web.controller                                  ;

import base.pojo.LoginInfo;
import base.service.IAccountService;
import base.service.IUserInfoService;
import common.RequireLogin;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope          ;
import org.springframework.stereotype.Controller             ;
import org.springframework.ui.Model                          ;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心
 * @author LiuZhi
 */
@Controller@Scope("prototype")
public class PersonalController
{
    @Autowired
    private IUserInfoService iUserInfoService;
    @Autowired
    private IAccountService  iAccountService ;

    /**
     * 请求个人中心页面
     */
    @RequireLogin()
    @RequestMapping("personal.html")
    public String personalCenter(Model model)
    {
        LoginInfo current   =  UserContext.getCurrentUser() ;
        model.addAttribute("userInfo"   ,iUserInfoService.get(current.getId()));
        model.addAttribute("accountInfo",iAccountService .get(current.getId()));
        return "personal";
    }
}
