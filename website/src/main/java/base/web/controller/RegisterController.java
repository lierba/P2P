package base.web.controller;

import common.Const;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import base.service.ILoginInfoService;

import javax.servlet.http.HttpServletRequest;

/**
 * 用于注册相关的控制器
 * @author LiuZhi
 */
@Controller@Scope("prototype")
public class RegisterController
{
    @Autowired
    ILoginInfoService iLoginInfoService;

    @ResponseBody
    @RequestMapping(value = "/register.do",method = RequestMethod.POST)
    public ServerResponse register(String username,String password)
    {
        try {
            return iLoginInfoService.register(username,password);
        } catch (Exception e) {
            return ServerResponse.createByErrorMsg("注册失败,异常信息:"+e.getMessage());
        }
    }
    @ResponseBody
    @RequestMapping(value = "/checkUsername.do",method = RequestMethod.POST)
    public ServerResponse checkUsername(String username)
    {
        return iLoginInfoService.checkUsername(username);
    }
    @ResponseBody
    @RequestMapping(value = "/login.do",method = RequestMethod.POST)
    public ServerResponse login(String username, String password, HttpServletRequest request){
        return iLoginInfoService.login(username,password,request.getRemoteAddr(), Const.USER_CUSTOMER);
    }
}
