package base.web.controller;

import base.pojo.IPlog;
import base.query.IPLogQuery;
import base.service.IiPlogService;
import com.github.pagehelper.PageInfo;
import common.RequireLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller@Scope("prototype")
public class IPLogController
{
    @Autowired
    IiPlogService iiPlogService;

    @RequireLogin
    @RequestMapping("ipLog.html")
    public String iplogList(IPLogQuery ipLogQuery, Model model)
    {
        PageInfo<IPlog> pi = iiPlogService.getAllIPLogByLimit(ipLogQuery);
        model.addAttribute("iq",ipLogQuery) ;
        model.addAttribute("pi",pi)         ;
        return "iplog_list"                              ;
    }
}
