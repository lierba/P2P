package base.service;

import common.ServerResponse;

import java.util.Map;

public interface IAliPayService
{
    ServerResponse pay(String money,String path);

    ServerResponse aliCallBack(Map<String,String> params) throws Exception;

    ServerResponse queryPay(String tradeCode);
}
