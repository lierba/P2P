package business.web.controller;

import base.query.CommonQuery;
import business.pojo.RechargeOffline;
import business.service.IRechargeOfflineService;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller@Scope("prototype")
public class RechargeOfflineController
{
    @Autowired
    private IRechargeOfflineService iRechargeOfflineService;

    @RequestMapping("allRechargeOffline.do")
    @ResponseBody
    public ServerResponse allRechargeOffline(CommonQuery commonQuery)
    {
        return iRechargeOfflineService.rechargeList(commonQuery);
    }
    @RequestMapping("getRechargeOffline.do")
    @ResponseBody
    public ServerResponse getRechargeOffline(Long id)
    {
        return iRechargeOfflineService.getRechargeOffline(id);
    }
    @RequestMapping("rechargeOfflineAuth.do")
    @ResponseBody
    public ServerResponse rechargeOfflineAuth(RechargeOffline rechargeOffline)
    {
        try {
            return iRechargeOfflineService.rechargeOfflineAuth(rechargeOffline);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
