package business.web.controller;

import business.pojo.PlatformBankInfo;
import business.service.IPlatformBankService;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller@Scope("prototype")
public class PlatformBankController
{
    @Autowired
    private IPlatformBankService iPlatformBankService;

    @RequestMapping("bankUpdateOrSave.do")
    @ResponseBody
    public ServerResponse updateOrSave(PlatformBankInfo platformBankInfo)
    {
        return iPlatformBankService.updateOrSave(platformBankInfo);
    }
    @RequestMapping("listPlatformBank.do")
    @ResponseBody
    public ServerResponse listPlatformBank()
    {
        return iPlatformBankService.listPlatformBank();
    }
    @RequestMapping("getPlatformBank.do")
    @ResponseBody
    public ServerResponse getPlatformBank(Long id)
    {
        return iPlatformBankService.getPlatformBank(id);
    }
}
