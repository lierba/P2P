package business.web.controller;

import business.query.BidRequestQuery;
import business.service.IBidRequestService;
import common.Const;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 借款相关控制器
 * @author LiuZhi
 */
@Controller@Scope("prototype")
public class BidRequestController
{
    @Autowired
    IBidRequestService iBidRequestService;

    @RequestMapping("allBidAgoAudit.do")
    @ResponseBody
    public ServerResponse allBidAgoAudit()
    {
       BidRequestQuery bidRequestQuery = new BidRequestQuery()                      ;
       bidRequestQuery.setBidRequestState(Const.BIDREQUEST_STATE_PUBLISH_PENDING)   ;
       return iBidRequestService.query(bidRequestQuery)                             ;
    }
    @RequestMapping("getBidAgoAudit.do")
    @ResponseBody
    public ServerResponse getBidAgoAudit(Long id)
    {
        return iBidRequestService.getBidAgoAudit(id) ;
    }
    @RequestMapping("bidAgoAudit.do")
    @ResponseBody
    public ServerResponse bidAgoAudit(Long id,String remark,int state)
    {
        return iBidRequestService.bidAgoAudit(id,remark,state) ;
    }
    @RequestMapping("borrowInfo.do")
    @ResponseBody
    public ServerResponse borrowInfo(Long id)
    {
        return iBidRequestService.borrowInfo(id) ;
    }

    @RequestMapping("allBidFullOneAudit.do")
    @ResponseBody
    public ServerResponse allBidFullOneAudit()
    {
        BidRequestQuery bidRequestQuery = new BidRequestQuery()                         ;
        bidRequestQuery.setBidRequestState(Const.BIDREQUEST_STATE_APPROVE_PENDING_ONE)  ;
        return iBidRequestService.query(bidRequestQuery)                                ;
    }
    @RequestMapping("allBidFullTwoAudit.do")
    @ResponseBody
    public ServerResponse allBidFullTwoAudit()
    {
        BidRequestQuery bidRequestQuery = new BidRequestQuery()                         ;
        bidRequestQuery.setBidRequestState(Const.BIDREQUEST_STATE_APPROVE_PENDING_TWO)  ;
        return iBidRequestService.query(bidRequestQuery)                                ;
    }
    @RequestMapping("bidFullOneAudit.do")
    @ResponseBody
    public ServerResponse bidFullOneAudit(Long id,String remark,int state)
    {
        try {
            return iBidRequestService.bidFullOneAudit(id,remark,state) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByError();
    }
    @RequestMapping("bidFullTwoAudit.do")
    @ResponseBody
    public ServerResponse bidFullTwoAudit(Long id,String remark,int state)
    {
        try {
            return iBidRequestService.bidFullTwoAudit(id,remark,state) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByError();
    }
}
