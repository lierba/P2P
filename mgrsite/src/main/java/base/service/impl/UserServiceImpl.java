package base.service.impl;

import base.mapper.UserInfoMapper;
import base.query.CommonQuery;
import base.service.IUserService;
import base.vo.UserListVo;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService
{
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    UserInfoMapper userInfoMapper;


    public ServerResponse allUser(CommonQuery commonQuery) {
        List<UserListVo> list = userInfoMapper.allUser(commonQuery);
        return ServerResponse.createBySuccess(list);
    }
}
