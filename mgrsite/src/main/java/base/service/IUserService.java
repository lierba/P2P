package base.service;

import base.query.CommonQuery;
import common.ServerResponse;

public interface IUserService
{

    ServerResponse allUser(CommonQuery commonQuery);
}
