package base.web.controller;

import base.pojo.RealAuth;
import base.pojo.VedioAuth;
import base.query.CommonQuery;
import base.service.IRealAuthService;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller@Scope("prototype")
public class RealAuthController
{
    @Autowired
    IRealAuthService iRealAuthService;

    @RequestMapping("allRealAuth.do")
    @ResponseBody
    public ServerResponse allRealAuth(CommonQuery commonQuery)
    {
        return iRealAuthService.allRealAuth(commonQuery);
    }
    @RequestMapping("getRealAuth.do")
    @ResponseBody
    public ServerResponse getRealAuth(Long id)
    {
        return ServerResponse.createBySuccess(iRealAuthService.getRealAuthFull(id));
    }
    @RequestMapping("RealAuth.do")
    @ResponseBody
    public ServerResponse realAuthAudit(RealAuth realAuth)
    {
        try {
            return iRealAuthService.realAuthAudit(realAuth);
        } catch (Exception e) {
            e.printStackTrace();
            return ServerResponse.createByErrorMsg(e.getMessage());
        }
    }
    //----------------------视频认证审核
    @RequestMapping("VedioAuth.do")
    @ResponseBody
    public ServerResponse vedioAuthAudit(VedioAuth vedioAuth)
    {
        try {
            return iRealAuthService.vedioAuthAudit(vedioAuth);
        } catch (Exception e) {
            e.printStackTrace();
            return ServerResponse.createByErrorMsg(e.getMessage());
        }
    }
    @RequestMapping("allVedioAuth.do")
    @ResponseBody
    public ServerResponse allVedioAuth(CommonQuery commonQuery)
    {
        return iRealAuthService.allVedioAuth(commonQuery);
    }
    @RequestMapping("getVedioAuth.do")
    @ResponseBody
    public ServerResponse getVedioAuth(Long id)
    {
        return ServerResponse.createBySuccess(iRealAuthService.getVedioAuth(id));
    }
    @RequestMapping("autoCompletion.do")
    @ResponseBody
    public ServerResponse autoCompletion(String text)
    {
        return iRealAuthService.autoCompletion(text);
    }
}
