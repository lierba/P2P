package base.web.controller;

import base.pojo.SystemDictionary;
import base.pojo.SystemDictionaryItem;
import base.service.ISystemDictionaryService;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller@Scope("prototype")
public class SystemDictionaryController
{
    @Autowired
    ISystemDictionaryService iSystemDictionaryService;

    @RequestMapping(value = "allSystemDictionary",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse allSystemDictionary(Integer pageNumber,Integer pageSize,String criteria)
    {
        return ServerResponse.createBySuccess(iSystemDictionaryService.allSystemDictionary(pageNumber,pageSize,criteria));
    }
    @RequestMapping(value = "addOrUpdateSystemDictionary",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse addOrUpdateSystemDictionary(SystemDictionary systemDictionary)
    {
        return iSystemDictionaryService.addOrUpdateSystemDictionary(systemDictionary);
    }
    @RequestMapping(value = "delSystemDictionary",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse delSystemDictionary(Long id)
    {
        return iSystemDictionaryService.delSystemDictionary(id);
    }
    @RequestMapping(value = "getSystemDictionary",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse getSystemDictionary(Long id)
    {
        return iSystemDictionaryService.getSystemDictionary(id);
    }
    @RequestMapping(value = "allSystemDictionaryItem",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse allSystemDictionaryItem(Integer pageNumber,Integer pageSize,String criteria,Long parentId)
    {
        return ServerResponse.createBySuccess(iSystemDictionaryService.allSystemDictionaryItem(pageNumber,pageSize,criteria,parentId));
    }
    @RequestMapping(value = "addOrUpdateSystemDictionaryItem",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse addOrUpdateSystemDictionaryItem(SystemDictionaryItem systemDictionaryItem)
    {
        return iSystemDictionaryService.addOrUpdateSystemDictionaryItem(systemDictionaryItem);
    }
    @RequestMapping(value = "getSystemDictionaryItem",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse getSystemDictionaryItem(Long id)
    {
        return iSystemDictionaryService.getSystemDictionaryItem(id);
    }
    @RequestMapping(value = "delSystemDictionaryItem",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse delSystemDictionaryItem(Long id)
    {
        return iSystemDictionaryService.delSystemDictionaryItem(id);
    }
}
