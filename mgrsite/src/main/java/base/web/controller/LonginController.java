package base.web.controller;

import base.service.ILoginInfoService;
import common.Const;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller@Scope("prototype")
public class LonginController
{
    @Autowired
    ILoginInfoService iLoginInfoService;
    @ResponseBody
    @RequestMapping(value = "/login.do",method = RequestMethod.POST)
    public ServerResponse login(String username, String password, HttpServletRequest request){
        return iLoginInfoService.login(username,password,request.getRemoteAddr(), Const.USER_MANAGE);
    }
}
