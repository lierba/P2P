package base.web.controller;

import base.query.CommonQuery;
import base.service.IUserService;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户管理控制器
 */
@Controller@Scope("prototype")
public class UserController
{
    @Autowired
    IUserService iUserService;

    @RequestMapping("allUser.do")
    @ResponseBody
    public ServerResponse allUser(CommonQuery commonQuery)
    {
        return iUserService.allUser(commonQuery);
    }
}
