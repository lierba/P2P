package base.web.controller;

import base.pojo.UserFile;
import base.query.CommonQuery;
import base.service.IUserFileService;
import common.RequireLogin;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller@Scope("prototype")
public class UserFileController
{
    @Autowired
    IUserFileService iUserFileService;

    @RequestMapping("allUserFileAuth.do")
    @ResponseBody
    public ServerResponse allUserFileAuth(CommonQuery commonQuery)
    {
        return iUserFileService.allUserFileAuth(commonQuery);
    }
    @RequestMapping("getUserFileAuth.do")
    @ResponseBody
    public ServerResponse getUserFileAuth(Long id)
    {
        return iUserFileService.getUserFileAuth(id);
    }
    @RequireLogin
    @RequestMapping("UserFileAudit.do")
    @ResponseBody
    public ServerResponse UserFileAudit(UserFile userFile)
    {
        try {
            return iUserFileService.UserFileAudit(userFile)       ;
        } catch (Exception e) {
            e.printStackTrace()                                   ;
            return ServerResponse.createByErrorMsg(e.getMessage());
        }
    }
}
