package listener;

import base.service.ILoginInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * 初始化超级管理员的监听器
 * @ContextRefreshedEvent 是监听Spring容器启动完成
 */
@Component
public class InitAdminListener implements ApplicationListener<ContextRefreshedEvent>
{
    @Autowired
    ILoginInfoService iLoginInfoService;

    public void onApplicationEvent(ContextRefreshedEvent event)
    {
        iLoginInfoService.checkAdmin();
    }
}
