package business.service;

import base.query.CommonQuery;
import business.pojo.RechargeOffline;
import common.ServerResponse;

public interface IRechargeOfflineService
{

    ServerResponse apply(RechargeOffline rechargeOffline);

    ServerResponse rechargeList(CommonQuery commonQuery);

    ServerResponse getRechargeOffline(Long id);

    ServerResponse rechargeOfflineAuth(RechargeOffline rechargeOffline) throws Exception;
}
