package business.service;

import business.pojo.RechargeOnline;
import common.ServerResponse;

public interface IRechargeOnlineService
{
    ServerResponse apply(RechargeOnline rechargeOnline) ;

    RechargeOnline getRechargeOnlineByTradeCode (String tradeCode)  ;

    void update(RechargeOnline r);
}
