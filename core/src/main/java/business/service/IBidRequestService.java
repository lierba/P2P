package business.service;

import business.pojo.BidRequest;
import business.pojo.BidRequestAuditHistory;
import business.query.BidRequestQuery;
import common.ServerResponse;

import java.math.BigDecimal;
import java.util.List;

public interface IBidRequestService
{
    ServerResponse  update              (BidRequest bidRequest)             ;

    boolean         canApplyBidRequest  ()                                  ;

    void            apply               (BidRequest bidRequest)             ;

    ServerResponse  query               (BidRequestQuery bidRequestQuery)   ;

    ServerResponse  getBidAgoAudit      (Long id)                           ;

    BidRequest      getBidRequest       (Long id)                           ;

    ServerResponse bidAgoAudit          (Long id,String remark,int state)   ;

    ServerResponse borrowInfo(Long id);

    List<BidRequestAuditHistory> listAuditHistoryByBidRequest(Long id);

    ServerResponse listAll(BidRequestQuery bidRequestQuery);

    ServerResponse bid(Long bidRequestId, BigDecimal amount) throws Exception;

    ServerResponse bidFullOneAudit(Long id, String remark, int state) throws Exception;

    ServerResponse bidFullTwoAudit(Long id, String remark, int state) throws Exception;
}
