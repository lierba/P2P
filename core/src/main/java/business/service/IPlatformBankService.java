package business.service;

import business.pojo.PlatformBankInfo;
import common.ServerResponse;

public interface IPlatformBankService
{
    ServerResponse listPlatformBank();

    ServerResponse updateOrSave(PlatformBankInfo platformBankInfo);

    ServerResponse getPlatformBank(Long id);
}
