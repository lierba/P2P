package business.service;

import business.pojo.BidRequest;
import business.pojo.SystemAccount;

import java.math.BigDecimal;

public interface ISystemAccountService
{
    void update(SystemAccount systemAccount);

    /**
     * 检查并初始化系统账户
     */
    void initAccount();

    /**
     * 系统账户收到借款管理费
     */
    void chargeBorrowFee(BidRequest br, BigDecimal manageChargeFee);

//    /**
//     * 系统账户收取提现手续费
//     */
//    void chargeWithdrawFee(MoneyWithdraw m);
//
//    /**
//     * 系统账户手续利息管理费
//     */
//    void chargeInterestFee(PaymentScheduleDetail psd,
//                           BigDecimal interestChargeFee);
}
