package business.service;

import base.pojo.Account;
import business.pojo.Bid;
import business.pojo.BidRequest;
import business.pojo.RechargeOffline;
import business.pojo.RechargeOnline;

import java.math.BigDecimal;

public interface IAccountFlowService
{
    void rechargeFlow(RechargeOffline old, Account applierAccount);
    void rechargeFlow(RechargeOnline old, Account applierAccount);

    void bid(Bid bid, Account currentAccount);

    void returnMoney(Bid bid, Account bidAccount);

    void borrowSuccess(BidRequest old, Account borrowAccount);

    void borrowChargeFee(BigDecimal manageChargeFee, BidRequest old, Account borrowAccount);

    void bidSuccess(Bid bid, Account bidAccount);
}
