package business.service.impl;

import base.pojo.Account;
import base.query.CommonQuery;
import base.service.IAccountService;
import business.mapper.RechargeOfflineMapper;
import business.pojo.RechargeOffline;
import business.service.IAccountFlowService;
import business.service.IRechargeOfflineService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import common.Const;
import common.ServerResponse;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.PageUtil;

import java.util.Date;
import java.util.List;

@Service
public class RechargeOfflineServiceImpl implements IRechargeOfflineService
{
    @Autowired
    RechargeOfflineMapper   rechargeOfflineMapper   ;
    @Autowired
    IAccountService         iAccountService         ;
    @Autowired
    IAccountFlowService     iAccountFlowService     ;

    public ServerResponse apply(RechargeOffline rechargeOffline)
    {
        rechargeOffline.setApplier(UserContext.getCurrentUser());
        rechargeOffline.setApplyTime(new Date())                ;
        rechargeOffline.setState(RechargeOffline.STATE_NORMAL)  ;
        if(rechargeOfflineMapper.insert(rechargeOffline) > 0){
            return ServerResponse.createBySuccess()             ;
        }
        return ServerResponse.createByError()                   ;
    }

    public ServerResponse rechargeList(CommonQuery commonQuery)
    {
        PageHelper.startPage(commonQuery.getPageNum(),commonQuery.getPageSize());
        if(UserContext.getCurrentUser() != null && UserContext.getCurrentUser().getUserType()== Const.USER_CUSTOMER){
        commonQuery.setApplierId(UserContext.getCurrentUser().getId());
        }
        List<RechargeOffline> list = rechargeOfflineMapper.query(commonQuery);
        return ServerResponse.createBySuccess(PageUtil.returnPageInfo(list));
    }

    public ServerResponse getRechargeOffline(Long id)
    {
        return ServerResponse.createBySuccess(rechargeOfflineMapper.selectByPrimaryKey(id));
    }

    public ServerResponse rechargeOfflineAuth(RechargeOffline rechargeOffline) throws Exception {
        RechargeOffline old = rechargeOfflineMapper.selectByPrimaryKey(rechargeOffline.getId());
        if(old !=null && old.getState() ==  RechargeOffline.STATE_NORMAL)
        {
            old.setAuditor(UserContext.getCurrentUser());
            old.setAuditTime(new Date())                ;
            old.setState(rechargeOffline.getState())    ;
            old.setRemark(rechargeOffline.getRemark())  ;
            if(rechargeOffline.getState()==RechargeOffline.STATE_AUDIT)
            {
                Account applierAccount = iAccountService.get(old.getApplier().getId());
                applierAccount.setUsableAmount(applierAccount.getUsableAmount().add(old.getAmount()));
                iAccountFlowService.rechargeFlow(old,applierAccount);
                iAccountService.update(applierAccount)  ;
            }
            rechargeOfflineMapper.updateByPrimaryKey(old);
            return ServerResponse.createBySuccess()     ;
        }
        return ServerResponse.createByError()           ;
    }
}
