package business.service.impl;

import base.pojo.Account;
import business.mapper.AccountFlowMapper;
import business.pojo.*;
import business.service.IAccountFlowService;
import common.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class AccountFlowServiceImpl implements IAccountFlowService
{
    @Autowired
    AccountFlowMapper accountFlowMapper;

    private AccountFlow createBaseFlow(Account account)
    {
        AccountFlow accountFlow = new AccountFlow()                             ;
        accountFlow.setAccountId(account.getId())                               ;
        accountFlow.setTradeTime(new Date())                                    ;
        accountFlow.setUsableAmount(account.getUsableAmount())                  ;
        accountFlow.setFreezeAmount(account.getFreezedAmount())                 ;
        return accountFlow                                                      ;
    }

    public void rechargeFlow(RechargeOffline r, Account account)
    {
        AccountFlow accountFlow = createBaseFlow(account)                       ;
        accountFlow.setAccountType(Const.ACCOUNT_ACTIONTYPE_RECHARGE_OFFLINE)   ;
        accountFlow.setAmount(r.getAmount())                                    ;
        accountFlow.setNote("线下充值成功,充值金额:"+r.getAmount())              ;
        accountFlowMapper.insert(accountFlow)                                   ;
    }

    public void rechargeFlow(RechargeOnline r, Account account) {
        AccountFlow accountFlow = createBaseFlow(account)                       ;
        accountFlow.setAccountType(Const.ACCOUNT_ACTIONTYPE_RECHARGE_OFFLINE)   ;
        accountFlow.setAmount(r.getAmount())                                    ;
        accountFlow.setNote("支付宝充值成功,充值金额:"+r.getAmount())            ;
        accountFlowMapper.insert(accountFlow)                                   ;
    }

    public void bid(Bid bid, Account account)
    {
        AccountFlow accountFlow = createBaseFlow(account)                       ;
        accountFlow.setAccountType(Const.ACCOUNT_ACTIONTYPE_BID_FREEZED)        ;
        accountFlow.setAmount(bid.getAvailableAmount())                         ;
        accountFlow.setNote("投标:"+bid.getBidRequestTitle()+
                                ",冻结账户余额:"+bid.getAvailableAmount())       ;
        accountFlowMapper.insert(accountFlow)                                   ;
    }

    public void returnMoney(Bid bid, Account account)
    {
        AccountFlow accountFlow = createBaseFlow(account)                       ;
        accountFlow.setAccountType(Const.ACCOUNT_ACTIONTYPE_BID_UNFREEZED)      ;
        accountFlow.setAmount(bid.getAvailableAmount())                         ;
        accountFlow.setNote("投标:"+bid.getBidRequestTitle()+
                ",满审拒绝退款:"+bid.getAvailableAmount())                       ;
        accountFlowMapper.insert(accountFlow)                                   ;
    }

    public void borrowSuccess(BidRequest old, Account account)
    {
        AccountFlow accountFlow = createBaseFlow(account)                           ;
        accountFlow.setAccountType(Const.ACCOUNT_ACTIONTYPE_BIDREQUEST_SUCCESSFUL)  ;
        accountFlow.setAmount(old.getBidRequestAmount())                            ;
        accountFlow.setNote("借款:"+old.getTitle() +
                ",成功,收到借款金额:"+old.getBidRequestAmount())                     ;
        accountFlowMapper.insert(accountFlow)                                       ;
    }

    public void borrowChargeFee(BigDecimal manageChargeFee, BidRequest old, Account account)
    {
        AccountFlow flow = createBaseFlow(account);
        flow.setAccountType(Const.ACCOUNT_ACTIONTYPE_CHARGE);
        flow.setAmount(manageChargeFee);
        flow.setNote("借款" + old.getTitle() + "成功,支付借款手续费:" + manageChargeFee);
        this.accountFlowMapper.insert(flow);

    }

    public void bidSuccess(Bid bid, Account account)
    {
        AccountFlow flow = createBaseFlow(account);
        flow.setAccountType(Const.ACCOUNT_ACTIONTYPE_BID_SUCCESSFUL);
        flow.setAmount(bid.getAvailableAmount());
        flow.setNote("投标" + bid.getBidRequestTitle() + "成功,取消投标冻结金额:"
                + bid.getAvailableAmount());
        this.accountFlowMapper.insert(flow);
    }
}
