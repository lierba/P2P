package business.service.impl;

import business.mapper.PlatformBankInfoMapper;
import business.pojo.PlatformBankInfo;
import business.service.IPlatformBankService;
import common.ServerResponse;
import lombok.experimental.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlatformBankServiceImpl implements IPlatformBankService
{
    @Autowired
    PlatformBankInfoMapper platformBankInfoMapper;
    public ServerResponse listPlatformBank()
    {
        return ServerResponse.createBySuccess(platformBankInfoMapper.selectAll());
    }

    public ServerResponse updateOrSave(PlatformBankInfo platformBankInfo)
    {
        if(platformBankInfo.getBankName().isEmpty() || platformBankInfo.getAccountName().isEmpty() || platformBankInfo.getAccountNumber().isEmpty() || platformBankInfo.getBankForkName().isEmpty())
        {
            return ServerResponse.createByErrorMsg("非法参数:参数为空");
        }
        int i;
        if(platformBankInfo.getId() != null)
        {
            i=platformBankInfoMapper.updateByPrimaryKey(platformBankInfo);
            if(i >0)
            {
                return ServerResponse.createBySuccess();
            }
            return ServerResponse.createByError();
        }
        i=platformBankInfoMapper.insert(platformBankInfo);
        if(i >0)
        {
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByError();
    }

    public ServerResponse getPlatformBank(Long id)
    {
        return ServerResponse.createBySuccess(platformBankInfoMapper.selectByPrimaryKey(id));
    }
}
