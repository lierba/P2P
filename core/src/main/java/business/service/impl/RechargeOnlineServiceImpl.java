package business.service.impl;

import base.service.IAccountService;
import business.mapper.RechargeOnlineMapper;
import business.pojo.RechargeOnline;
import business.service.IAccountFlowService;
import business.service.IRechargeOnlineService;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RechargeOnlineServiceImpl implements IRechargeOnlineService
{
    @Autowired
    RechargeOnlineMapper    rechargeOnlineMapper    ;
    @Autowired
    IAccountService         iAccountService         ;
    @Autowired
    IAccountFlowService     iAccountFlowService     ;


    public ServerResponse apply(RechargeOnline rechargeOnline)
    {
        return ServerResponse.createBySuccess(rechargeOnlineMapper.insert(rechargeOnline));
    }

    public RechargeOnline getRechargeOnlineByTradeCode(String tradeCode)
    {
        return rechargeOnlineMapper.selectByTradeCode(tradeCode);
    }

    public void update(RechargeOnline r)
    {
        rechargeOnlineMapper.updateByPrimaryKey(r);
    }
}
