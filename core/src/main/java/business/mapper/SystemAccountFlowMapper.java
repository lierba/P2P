package business.mapper;

import business.pojo.SystemAccountFlow;
import java.util.List;

public interface SystemAccountFlowMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(SystemAccountFlow record);

    SystemAccountFlow selectByPrimaryKey(Long id);

    List<SystemAccountFlow> selectAll();

    int updateByPrimaryKey(SystemAccountFlow record);
}