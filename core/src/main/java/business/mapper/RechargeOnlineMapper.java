package business.mapper;

import business.pojo.RechargeOnline;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RechargeOnlineMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(RechargeOnline record);

    RechargeOnline selectByPrimaryKey(Long id);

    List<RechargeOnline> selectAll();

    int updateByPrimaryKey(RechargeOnline record);

    RechargeOnline selectByTradeCode(@Param("tradeCode") String tradeCode);
}