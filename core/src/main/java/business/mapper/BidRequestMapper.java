package business.mapper;

import business.pojo.BidRequest;
import business.query.BidRequestQuery;

import java.util.List;

public interface BidRequestMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(BidRequest record);

    BidRequest selectByPrimaryKey(Long id);

    List<BidRequest> selectAll(BidRequestQuery bidRequestQuery);

    int updateByPrimaryKey(BidRequest record);

    List<BidRequest> query(BidRequestQuery bidRequestQuery);

}