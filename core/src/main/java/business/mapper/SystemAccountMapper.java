package business.mapper;

import business.pojo.SystemAccount;
import java.util.List;

public interface SystemAccountMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(SystemAccount record);

    SystemAccount selectByPrimaryKey(Long id);

    List<SystemAccount> selectAll();

    int updateByPrimaryKey(SystemAccount record);

    SystemAccount selectCurrent();
}