package business.mapper;

import business.pojo.BidRequestAuditHistory;
import java.util.List;

public interface BidRequestAuditHistoryMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(BidRequestAuditHistory record);

    BidRequestAuditHistory selectByPrimaryKey(Long id);

    List<BidRequestAuditHistory> selectAll();

    int updateByPrimaryKey(BidRequestAuditHistory record);

    List<BidRequestAuditHistory> listByBidRequest(Long id);
}