package business.mapper;

import base.query.CommonQuery;
import business.pojo.RechargeOffline;
import common.ServerResponse;

import java.util.List;

public interface RechargeOfflineMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(RechargeOffline record);

    RechargeOffline selectByPrimaryKey(Long id);

    List<RechargeOffline> selectAll();

    int updateByPrimaryKey(RechargeOffline record);

    List<RechargeOffline> query(CommonQuery commonQuery);
}