package business.mapper;

import business.pojo.PlatformBankInfo;
import java.util.List;

public interface PlatformBankInfoMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(PlatformBankInfo record);

    PlatformBankInfo selectByPrimaryKey(Long id);

    List<PlatformBankInfo> selectAll();

    int updateByPrimaryKey(PlatformBankInfo record);
}