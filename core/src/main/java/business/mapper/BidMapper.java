package business.mapper;

import business.pojo.Bid;

import java.util.List;

public interface BidMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(Bid record);

    Bid selectByPrimaryKey(Long id);

    List<Bid> selectAll();

    int updateByPrimaryKey(Bid record);
}