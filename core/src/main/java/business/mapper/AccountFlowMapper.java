package business.mapper;

import business.pojo.AccountFlow;
import java.util.List;

public interface AccountFlowMapper
{
    int deleteByPrimaryKey(Long id);

    int insert(AccountFlow record);

    AccountFlow selectByPrimaryKey(Long id);

    List<AccountFlow> selectAll();

    int updateByPrimaryKey(AccountFlow record);
}