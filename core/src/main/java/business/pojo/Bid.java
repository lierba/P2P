package business.pojo;

import base.pojo.BasePojo;
import base.pojo.LoginInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 一次投标对象
 */
@Data
public class Bid extends BasePojo
{
    private BigDecimal  actualRate      ;//年利率
    private BigDecimal  availableAmount ;//投标金额
    private Long        bidRequestId    ;//借款标ID
    private String      bidRequestTitle ;//标的名称
    private LoginInfo   bidUser         ;//投标人
    private Date        bidTime         ;//投标时间
    private int         bidRequestState ;//标的状态

}
