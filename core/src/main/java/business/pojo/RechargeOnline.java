package business.pojo;

import base.pojo.BasePojo;
import base.pojo.LoginInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 线上充值
 */
@Data
public class RechargeOnline extends BasePojo
{
    private LoginInfo           applier         ;//充值人
    private String              tradeCode       ;//订单号
    private String              tradeNO         ;//交易号
    private Date                rechargeDate    ;//创建时间
    private BigDecimal          amount          ;//充值金额
    private String              qrPath          ;//二维码
    private int                 state           ;//状态
}
