package business.pojo;


import base.pojo.BasePojo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class SystemAccountFlow extends BasePojo
{
    private Date        createdDate      ;// 流水创建时间
    private int         accountActionType;// 系统账户流水类型
    private BigDecimal  amount           ;// 流水相关金额
    private String      note             ;
    private BigDecimal  balance          ;// 流水产生之后系统账户的可用余额;
    private BigDecimal  freezeAmount     ;// 流水产生之后系统账户的冻结余额;
    private Long        systemAccountId  ;// 对应的系统账户的id
}