package business.pojo;

import base.pojo.BasePojo;
import base.pojo.LoginInfo;
import business.util.DecimalFormatUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import common.Const;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

/**
 * 借款对象
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class BidRequest extends BasePojo
{
    private int         version                 ;//版本号
    private int         returnType              ;//还款方式
    private int         bidRequestType          ;//借款类型
    private int         bidRequestState         ;//状态
    private BigDecimal  bidRequestAmount        ;//借款的金额
    private BigDecimal  currentRate             ;//借款的利率(年利率)
    private BigDecimal  minBidAmount            ;//借款允许的最小的投标金额
    private int         monthsReturn            ;//借款期限
    private int         bidCount                ;//投标次数
    private BigDecimal  totalRewardAmount       ;//总回报金额
    private BigDecimal  currentSum = Const.Zero ;//已投金额
    private String      title                   ;//借款标题
    private String      description             ;//借款描述
    private String      note                    ;//风控评审意见
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date        disableDate             ;//招标到期时间
    private int         disableDays             ;//招标天数
    private LoginInfo   createUser              ;//借款人
    private List<Bid>   bids                    ;//投标记录
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date        applyTime               ;//申请时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date        publishTime             ;//发布时间

    public String getBidRequestStateDisplay()
    {
        switch (bidRequestState)
        {
            case Const.BIDREQUEST_STATE_PUBLISH_PENDING        : return "待发布"          ;
            case Const.BIDREQUEST_STATE_BIDDING                : return "招标中"          ;
            case Const.BIDREQUEST_STATE_UNDO                   : return "已撤销"          ;
            case Const.BIDREQUEST_STATE_BIDDING_OVERDUE        : return "流标"            ;
            case Const.BIDREQUEST_STATE_APPROVE_PENDING_ONE    : return "满标一审"        ;
            case Const.BIDREQUEST_STATE_APPROVE_PENDING_TWO    : return "满标二审"        ;
            case Const.BIDREQUEST_STATE_REJECTED               : return "满标审核被拒绝"  ;
            case Const.BIDREQUEST_STATE_PAYING_BACK            : return "还款中"         ;
            case Const.BIDREQUEST_STATE_COMPLETE_PAY_BACK      : return "完成"           ;
            case Const.BIDREQUEST_STATE_PAY_BACK_OVERDUE       : return "逾期"           ;
            case Const.BIDREQUEST_STATE_PUBLISH_REFUSE         : return "发标拒绝"       ;
            default                                            : return ""               ;
        }
    }
    public String getBidRequestTypeDisplay()
    {
        switch (bidRequestType)
        {
            case Const.BIDREQUEST_TYPE_NORMAL : return "信用标";
            default                           : return ""      ;
        }
    }
    public String getReturnTypeDisplay()
    {
        switch (returnType)
        {
            case Const.RETURN_TYPE_MONTH_INTEREST_PRINCIPAL : return "按月分期";
            case Const.RETURN_TYPE_MONTH_INTEREST           : return "按月到期";
            default                                         : return ""      ;
        }
    }

    /**
     * 计算还需金额
     */
    public BigDecimal getRemainAmount()
    {
        return DecimalFormatUtil.formatBigDecimal(bidRequestAmount.subtract(currentSum),Const.DISPLAY_SCALE);
    }
    /**
     * 计算当前投标进度
     */
    public BigDecimal getPersent()
    {
        return currentSum.divide(bidRequestAmount,Const.DISPLAY_SCALE, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
    }
}
