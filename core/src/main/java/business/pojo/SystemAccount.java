package business.pojo;

import base.pojo.BasePojo;
import common.Const;
import lombok.Data;

import java.math.BigDecimal;
@Data
public class SystemAccount extends BasePojo{

    private int         version                     ;// 版本
    private BigDecimal  usableAmount    = Const.Zero;// 平台账户剩余金额
    private BigDecimal  freezeAmount   = Const.Zero;// 平台账户冻结金额

}