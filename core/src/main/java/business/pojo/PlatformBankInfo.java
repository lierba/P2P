package business.pojo;

import base.pojo.BasePojo;
import lombok.Data;

@Data
public class PlatformBankInfo extends BasePojo
{
    private String bankName     ;//银行名称
    private String accountName  ;//开户人姓名
    private String accountNumber;//银行账号
    private String bankForkName ;//开户支行
}
