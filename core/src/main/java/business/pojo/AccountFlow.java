package business.pojo;

import base.pojo.BasePojo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 账户交易流水
 * @author LiuZhi
 */
@Data
public class AccountFlow extends BasePojo
{
    private Long        accountId   ;//关联账户
    private BigDecimal  amount      ;//变化金额
    private Date        tradeTime   ;//变化时间
    private int         accountType ;//变化类型
    private BigDecimal  usableAmount;//剩余金额
    private BigDecimal  freezeAmount;//冻结金额
    private String      note        ;//流水说明
}
