package business.pojo;

import base.pojo.BaseAudit;
import base.pojo.BasePojo;
import base.pojo.LoginInfo;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 线下充值
 */
@Data
public class RechargeOffline extends BaseAudit
{
    private PlatformBankInfo    bankInfo        ;//充值银行卡号
    private String              tradeCode       ;//交易号
    private BigDecimal          amount          ;//充值金额
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date                tradeTime       ;//充值时间
    private String              note            ;//充值说明
}
