package business.pojo;

import base.pojo.BaseAudit;
import lombok.Data;

@Data
public class BidRequestAuditHistory extends BaseAudit
{
    /**
     * 发标前审核
     */
    public static final int PUBLISH_AUDIT   =   0  ;
    /**
     * 满标一审
     */
    public static final int FULL_AUDIT_ONE  =   1  ;
    /**
     * 满标二审
     */
    public static final int FULL_AUDIT_TWO =   2  ;

    private Long    binRequestId;
    private int     auditType   ;

    public String getAuditTypeDisplay()
    {
        switch (this.auditType)
        {
            case PUBLISH_AUDIT  :return "发标前审核";
            case FULL_AUDIT_ONE :return "满标一审"  ;
            case FULL_AUDIT_TWO :return "满标二审"  ;
            default             :return ""          ;
        }
    }
}
