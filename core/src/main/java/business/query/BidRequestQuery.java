package business.query;

import base.query.CommonQuery;
import lombok.Data;

@Data
public class BidRequestQuery extends CommonQuery
{
    private int     bidRequestState     = -1    ; //借款状态
    private int[]   bidRequestStates            ;//要查询的多个借款状态
}
