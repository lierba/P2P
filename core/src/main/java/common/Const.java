package common;


import lombok.Data;

import java.math.BigDecimal;
@Data
public class Const
{
    //---------------------------------------------------------------
    /**
     * 当前用户登录的SESSION
     */
    public static final String      USER_IN_SESSION     =   "loginInfo" ;
    /**
     * 当前验证码的SESSION
     */
    public static final String      VERIFY_CODE_SESSION =   "verifyCode";
    //---------------------------------------------------------------
    /**
     * 前台或后台登录 0 代表前台 1代表后台
     */
    public static final int         USER_CUSTOMER       =   0           ;
    public static final int         USER_MANAGE         =   1           ;
    //---------------------------------------------------------------
    public static final String      ADMIN_USERNAME      =   "admin"     ;
    public static final String      ADMIN_PASSWORD      =   "admin"     ;
    //---------------------------------------------------------------
    /**
     * 登录日记的常量 0-->成功,1-->失败
     */
    public static final int         STATE_SUCCESS       =   0           ;
    public static final int         STATE_ERROR         =   1           ;
    //---------------------------------------------------------------

    /**
     * 用户登录状态 0-->正常,1-->锁定
     */
    public static final int         STATE_NORMAL        =   0           ;
    public static final int         STATE_LOCK          =   1           ;
    //---------------------------------------------------------------
    /**
     *定义计算精度
     */
    public static final int         CAL_SCALE           =   8           ;
    /**
     *定义显示精度
     */
    public static final int         DISPLAY_SCALE       =   2           ;
    /**
     * 定义存储精度
     */
    public static final int         STORE_SCALE         =   4           ;
    /**
     * 定义存储精度
     */
    public static final BigDecimal  Zero                =   new BigDecimal("0.0000")    ;
    /**
     * 定义初始授信额度
     */
    public static final BigDecimal  INIT_BORROW_LIMIT   =   new BigDecimal("5000.0000") ;
    //---------------------------------------------------------------
    /**
     * 用户绑定手机状态码
     */
    public static final Long        OP_BIND_PHONE       =  1L<<0                            ;
    /**
     * 用户绑定邮箱状态码
     */
    public static final Long        OP_BIND_EMAIL       =  1L<<1                            ;
    /**
     * 用户绑定实名认证状态码
     */
    public static final Long        OP_REAL_AUTH        =  1L<<2                            ;
    /**
     * 用户是否是VIP状态码
     */
    public static final Long        OP_VIP              =  1L<<3                            ;
    /**
     * 用户是否是填写了基本资料
     */
    public static final Long        OP_BASE_INFO        =  1L<<4                            ;
    /**
     * 用户是否是视频认证
     */
    public static final Long        OP_VEDIO_AUTH       =  1L<<5                            ;
    /**
     * 还有一个借款在流程中
     */
    public static final Long        OP_HAS_BIDREQUEST   =  1L<<6                            ;
    //---------------------------------------------------------------
    /**
     * 基本的认证分数(30分)
     */
    public static final int         BASE_SCORE          =  30                               ;
    //---------------------------------------------------------------还款方式
    /**
     * 按月分期还款(等额本息)
     */
    public static final int         RETURN_TYPE_MONTH_INTEREST_PRINCIPAL=  0                ;                            ;
    /**
     * 按月到期还款(每月还利息,到期还本息)
     */
    public static final int         RETURN_TYPE_MONTH_INTEREST          =  1                ;                            ;
    //---------------------------------------------------------------
    /**
     * 普通信用标
     */
    public final static int         BIDREQUEST_TYPE_NORMAL              =  0                ;
    //---------------------------------------------------------------标的状态
    /**
     * 待发布：借款刚提交；
     */
    public static final int         BIDREQUEST_STATE_PUBLISH_PENDING    =   0               ;
    /**
     * 招标中：发标审核通过；
     */
    public static final int         BIDREQUEST_STATE_BIDDING            =   1               ;
    /**
     *已撤销：在发标过程中，如果借款用户撤销了借款或者后台管理员撤销了借款；
     */
    public static final int         BIDREQUEST_STATE_UNDO               =   2               ;
    /**
     *流标：在招标时间内，标的没有投满；
     */
    public static final int         BIDREQUEST_STATE_BIDDING_OVERDUE    =   3               ;
    /**
     *满标1审：满标后进入满标一审（待审）；
     */
    public static final int         BIDREQUEST_STATE_APPROVE_PENDING_ONE=   4               ;
    /**
     *满标2审：通过满标一审，进入满标二审（待审）；
     */
    public static final int         BIDREQUEST_STATE_APPROVE_PENDING_TWO=   5               ;
    /**
     *满标审核被拒绝：满标一审或者满标二审失败；
     */
    public static final int         BIDREQUEST_STATE_REJECTED           =   6               ;
    /**
     *还款中：通过满标二审，进入还款状态
     */
    public static final int         BIDREQUEST_STATE_PAYING_BACK        =   7               ;
    /**
     *已还清：所有还款全部还清；
     */
    public static final int         BIDREQUEST_STATE_COMPLETE_PAY_BACK  =   8               ;
    /**
     *逾期：如果某一期还款逾期，整个标都进入逾期状态；
     */
    public static final int         BIDREQUEST_STATE_PAY_BACK_OVERDUE   =   9               ;
    /**
     *初审拒绝状态：发标前审核失败；
     */
    public static final int         BIDREQUEST_STATE_PUBLISH_REFUSE     =   10              ;
    //---------------------------------------------------------------
    /**
     * 系统规定的最小投标金额
     */
    public static final BigDecimal  SMALLEST_BID_AMOUNT                 = new BigDecimal(
            "50.0000");
    /**
     * 系统规定的最小借款金额
     */
    public static final BigDecimal  SMALLEST_BIDREQUEST_AMOUNT          = new BigDecimal(
            "500.0000");
    /**
     * 系统最小借款利息
     */
    public static final BigDecimal  SMALLEST_CURRENT_RATE               = new BigDecimal(
            "5.0000");
    /**
     * 系统最大借款利息
     */
    public static final BigDecimal  MAX_CURRENT_RATE                    = new BigDecimal
            ("20.0000");

    /**
     * 系统最小提现金额
     */
    public static final BigDecimal  MIN_WITHDRAW_AMOUNT                 = new BigDecimal(
            "500.0000");
    /**
     * 系统提现手续费
     */
    public static final BigDecimal  MONEY_WITHDRAW_CHARGEFEE            = new BigDecimal(
            "2.0000");
    //---------------------------------------------------------------资金流水类别
    /**
     * 线下充值 可用余额增加
     */
    public final static int         ACCOUNT_ACTIONTYPE_RECHARGE_OFFLINE         = 0     ;
    /**
     * 提现成功 冻结金额减少
     */
    public final static int         ACCOUNT_ACTIONTYPE_WITHDRAW                 = 1     ;
    /**
     * 成功借款 可用余额增加
     */
    //
    public final static int         ACCOUNT_ACTIONTYPE_BIDREQUEST_SUCCESSFUL    = 2     ;
    /**
     * 成功投标
     */
    public final static int         ACCOUNT_ACTIONTYPE_BID_SUCCESSFUL           = 3     ;
    /**
     * 还款
     */
    public final static int         ACCOUNT_ACTIONTYPE_RETURN_MONEY             = 4     ;
    /**
     * 回款
     */
    public final static int         ACCOUNT_ACTIONTYPE_CALLBACK_MONEY           = 5     ;
    /**
     * 支付平台管理费
     */
    public final static int         ACCOUNT_ACTIONTYPE_CHARGE                   = 6     ;
    /**
     * 利息管理费
     */
    public final static int         ACCOUNT_ACTIONTYPE_INTEREST_SHARE           = 7     ;
    /**
     * 提现手续费
     */
    public final static int         ACCOUNT_ACTIONTYPE_WITHDRAW_MANAGE_CHARGE   = 8     ;
    /**
     * 充值手续费
     */
    public final static int         ACCOUNT_ACTIONTYPE_RECHARGE_CHARGE          = 9     ;
    /**
     * 投标冻结金额
     */
    public final static int         ACCOUNT_ACTIONTYPE_BID_FREEZED              = 10    ;
    /**                                                                                 
     * 取消投标冻结金额                                                                         
     */                                                                                 
    public final static int         ACCOUNT_ACTIONTYPE_BID_UNFREEZED            = 11    ;
    /**                                                                                 
     * 提现申请冻结金额                                                                         
     */                                                                                 
    public final static int         ACCOUNT_ACTIONTYPE_WITHDRAW_FREEZED         = 12    ;
    /**
     * 提现申请失败取消冻结金额
     */
    public final static int         ACCOUNT_ACTIONTYPE_WITHDRAW_UNFREEZED       = 13    ;
    /**
     * 线上充值
     */
    public final static int         ACCOUNT_ACTIONTYPE_RECHARGE_ONLINE         = 14     ;
    //---------------------------------------------------------------
    /**
     * 系统账户收到账户管理费（借款管理费）
     */
    public final static int         SYSTEM_ACCOUNT_ACTIONTYPE_MANAGE_CHARGE             = 1     ;
    /**
     * 系统账户收到利息管理费
     */
    public final static int         SYSTEM_ACCOUNT_ACTIONTYPE_INTREST_MANAGE_CHARGE     = 2     ;
    /**
     * 系统账户收到提现手续费
     */
    public final static int         SYSTEM_ACCOUNT_ACTIONTYPE_WITHDRAW_MANAGE_CHARGE    = 3     ;
    //---------------------------------------------------------------
    /**
     * 正常待还
     */
    public final static int         PAYMENT_STATE_NORMAL                                = 0     ;
    /**
     * 已还
     */
    public final static int         PAYMENT_STATE_DONE                                  = 1     ;
    /**
     * 逾期
     */
    public final static int         PAYMENT_STATE_OVERDUE                               = 2     ;
    //---------------------------------------------------------------
    /**
     * 未支付
     */
    public final static int         PAY_NO_PAY                                  = 0     ;
    /**
     * 已支付
     */
    public final static int         PAY_SUCCESS                                 = 1     ;
    /**
     * 支付取消
     */
    public final static int         PAY_CANCELED                                = 2     ;
    /**
     * 支付关闭
     */
    public final static int         PAY_CLOSE                                   = 3     ;
    public interface                aliPayCallBack
    {

        String TRADE_STATUS_WAIT_BUYER_PAY       =   "WAIT_BUYER_PAY";
        String TRADE_STATUS_TRADE_CLOSED         =   "TRADE_CLOSED"  ;
        String TRADE_STATUS_TRADE_SUCCESS        =   "TRADE_SUCCESS" ;
        String TRADE_STATUS_TRADE_FINISHED       =   "TRADE_FINISHED";

        String RESPONSE_SUCCESS                  =  "success"        ;
        String RESPONSE_FAILED                   =  "failed"         ;
    }
    //---------------------------------------------------------------
    public final static String      MASTER_DS                                   = "masterDS"  ;
    public final static String      SLAVE_DS                                    = "slaveDS"   ;

}
