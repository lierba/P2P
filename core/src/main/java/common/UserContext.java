package common;

import base.pojo.LoginInfo;
import base.vo.VerifyCodeVo;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

/**
 * 用于存放当前用户的上下文
 * @author LiuZhi
 */
public class UserContext
{
    /**
     * 反向获取request方法
     * 具体需查看RequestContextListener.requestInitialized打包过程
     */
    private static HttpSession  getSession()
    {
        return ((ServletRequestAttributes)RequestContextHolder
                .getRequestAttributes()).getRequest().getSession();
    }
    public static void          putCurrentUser(LoginInfo loginInfo)
    {
        getSession().setAttribute(Const.USER_IN_SESSION,loginInfo);
    }
    public static LoginInfo     getCurrentUser()
    {
        return (LoginInfo) getSession().getAttribute(Const.USER_IN_SESSION);
    }
    public static void          putVerifyCodeVo(VerifyCodeVo verifyCodeVo)
    {
        getSession().setAttribute(Const.VERIFY_CODE_SESSION,verifyCodeVo);
    }
    public static VerifyCodeVo  getVerifyCodeVo()
    {
        return (VerifyCodeVo)getSession().getAttribute(Const.VERIFY_CODE_SESSION);
    }
}
