package common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;

/**
 * 通用的结果集返回
 * @author LiuZhi
 */
@Data
@Getter@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServerResponse<T>
{
    private Integer     status  ;
    private String      msg     ;
    private T           data    ;

    private ServerResponse(int status)
    {
        this.status =   status  ;
    }
    private ServerResponse(int status,T data)
    {
        this.status =   status  ;
        this.data   =   data    ;
    }
    private ServerResponse(int status,String msg)
    {
        this.status =   status  ;
        this.msg    =   msg     ;
    }
    private ServerResponse(int status,String msg,T data)
    {
        this.status =   status  ;
        this.msg    =   msg     ;
        this.data   =   data    ;
    }

    /**
     * 判断是否成功
     */
    @JsonIgnore
    public boolean isSuccess()
    {
        return this.status==ResponseCode.SUCCESS.getCode();
    }

    public static <T> ServerResponse<T> createBySuccess()
    {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode())            ;
    }
    public static <T> ServerResponse<T> createBySuccessMsg(String msg)
    {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg)        ;
    }
    public static <T> ServerResponse<T> createBySuccess(T data)
    {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data)       ;
    }
    public static <T> ServerResponse<T> createBySuccess(String msg,T data)
    {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg,data)   ;
    }
    public static <T> ServerResponse<T> createByError()
    {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
    }
    public static <T> ServerResponse<T> createByErrorMsg(String msg)
    {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),msg)              ;
    }
    public static <T> ServerResponse<T> createByErrorCodeMsg(int code,String msg)
    {
        return new ServerResponse<T>(code,msg)                                      ;
    }
    public static <T> ServerResponse<T> createByErrorResponseCode(ResponseCode responseCode)
    {
        return  new ServerResponse<T>(responseCode.getCode(),responseCode.getDesc());
    }
}
