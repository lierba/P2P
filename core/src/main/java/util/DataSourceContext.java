package util;


/**
 * 存放当前线程需要访问的DS的名字
 */
public class DataSourceContext
{
    private static ThreadLocal<String> dataSourcePool = new ThreadLocal<>();

    public static void set(String dsName)
    {
        dataSourcePool.set(dsName);
    }

    public static String get()
    {
       return dataSourcePool.get();
    }
}
