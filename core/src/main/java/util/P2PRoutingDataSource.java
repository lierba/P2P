package util;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * P2P中的路由DataSource
 */
public class P2PRoutingDataSource extends AbstractRoutingDataSource
{
    protected Object determineCurrentLookupKey()
    {
        return DataSourceContext.get();
    }
}
