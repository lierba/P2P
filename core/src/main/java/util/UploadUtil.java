package util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 处理文件上传的工具类
 */
public class UploadUtil
{
    /**
     * @param file      上传的文件
     * @param basePath  上传的路径
     * @return  处理后的文件名字
     */
    public static String upload(MultipartFile file,String basePath)
    {
        String orgFileName  = file.getOriginalFilename()                ;//得到之前的文件全名
        String fileName     = UUID.randomUUID().toString() + "."
                           + FilenameUtils.getExtension(orgFileName)    ;//生成随机名字并加上原后缀名
        try {
        File targetFile     = new File(basePath,fileName)               ;//生成一个文件对象,并赋值地址和名字
            FileUtils.writeByteArrayToFile(targetFile,file.getBytes())  ;//把file的二进制内容写到target里面
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }
}
