package util;

import com.github.pagehelper.PageInfo;

import java.util.List;


public class PageUtil
{

    /**
     * 判断是否pageNum是否为空,是则返回1,否则原值返回
     */
    public static int getPageNum(Integer pageNum)
    {
        return pageNum  ==  null    ?   1   :  pageNum;
    }
    /**
     * 判断是否pageSize是否为空,是则返回10,否则原值返回
     */
    public static int getPageSize(Integer pageSize)
    {
        return pageSize  ==  null    ?   10 :  pageSize;
    }

    public static PageInfo returnPageInfo(List list)
    {
        if(list == null || list.size() <= 0)
        {
            return  null;
        }
        PageInfo pageInfo =new PageInfo(list);
        return pageInfo                      ;
    }
}
