package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import common.Const;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录日记(是否登录成功都需要被记录)
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class IPlog extends BasePojo implements Serializable
{
    private String  userName    ;//记录用户名
    private String  ip          ;//记录用户IP
    private Date    loginTime   ;//记录用户登录时间
    private int     state       ;//记录登录结果 0-->成功,1-->失败.
    private int     userType    ;//记录登录类型 0-->前台,1-->后台.

    public String getStateDisplay()
    {
        return state== Const.STATE_SUCCESS?"登录成功":"登录失败";
    }
    public String getuserTypeDisplay()
    {
        return userType== Const.USER_CUSTOMER?"前台用户":"后台管理员";
    }
}
