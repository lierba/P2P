package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 风控材料
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserFile extends BaseAudit implements Serializable
{
    private String                  image       ;//风控材料图片
    private SystemDictionaryItem    fileTypes   ;//风控材料分类
    private int                     score       ;//风控材料评分
}
