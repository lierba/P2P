package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 数据字典的分类
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class SystemDictionary extends BasePojo implements Serializable
{
    private String  sn   ;
    private String  title;
}
