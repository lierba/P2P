package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 邮箱验证
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class MailVerify extends BasePojo implements Serializable
{
    private long    userInfoId  ;
    private String  email       ;
    private Date    deadline    ;
    private String  randomCode  ;
}
