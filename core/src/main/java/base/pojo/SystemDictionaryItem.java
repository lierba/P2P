package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据字典的分类
 * @author LiuZhi 
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class SystemDictionaryItem extends BasePojo implements Serializable
{
    private String  title   ;
    private Long    parentId;
    private int     sequence;
}
