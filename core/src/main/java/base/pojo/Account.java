package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import common.Const;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户对应的账户信息
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
@SuppressWarnings("SpellCheckingInspection")
public class Account extends BasePojo implements Serializable
{
    private int         version                                         ;
    private String      tradePassword                                   ;//账户交易密码
    private BigDecimal  usableAmount        = Const.Zero                ;//账户可用余额
    private BigDecimal  freezedAmount       = Const.Zero                ;//账户冻结金额
    private BigDecimal  unReceiveInterest   = Const.Zero                ;//账户待收利息
    private BigDecimal  unReceivePrincipal  = Const.Zero                ;//账户待收本金
    private BigDecimal  unReturnAmount      = Const.Zero                ;//账户待还金额
    private BigDecimal  remainBorrowLimit   = Const.INIT_BORROW_LIMIT   ;//账户剩余额度
    private BigDecimal  borrowLimit         = Const.INIT_BORROW_LIMIT   ;//账户授信额度


    /**
     * 返回账户总额
     */
    public  BigDecimal  getTotalAmount()
    {
        return usableAmount.add(freezedAmount).add(unReceivePrincipal);
    }
}
