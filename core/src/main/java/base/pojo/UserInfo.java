package base.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import common.Const;
import lombok.Data;
import util.BitOperationUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户相关信息
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserInfo extends BasePojo implements Serializable
{
    private int                  version            ;
    private long                 bitState=0         ;//用作位运算,必须要设置为0,否则为NULL无法计算
    private String               realName           ;
    private String               idNumber           ;
    private String               phoneNumber        ;
    private String               email              ;
    private int                  score              ;//风控累计分数
    private Long                 realAuthId         ;//该用户对应的实名认证对象ID
    private Long                 vedioAuthId        ;//该用户对应的视频认证对象ID
    private SystemDictionaryItem incomeGrade        ;
    private SystemDictionaryItem marriage           ;
    private SystemDictionaryItem kidCount           ;
    private SystemDictionaryItem educationBackground;
    private SystemDictionaryItem houseCondition     ;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date                 createTime         ;
    public  boolean getHasBindPhone()
    {
        return BitOperationUtil.hasState(bitState, Const.OP_BIND_PHONE) ;
    }
    public  boolean getHasBindEmail()
    {
        return BitOperationUtil.hasState(bitState, Const.OP_BIND_EMAIL) ;
    }
    public  boolean getHasRealAuth()
    {
        return BitOperationUtil.hasState(bitState, Const.OP_REAL_AUTH)  ;
    }
    public  boolean getHasVIP()
    {
        return BitOperationUtil.hasState(bitState, Const.OP_VIP)        ;
    }
    public  boolean getHasBaseInfo()
    {
       return BitOperationUtil.hasState(bitState, Const.OP_BASE_INFO)   ;
    }
    public  boolean getHasVedioAuth()
    {
       return BitOperationUtil.hasState(bitState, Const.OP_VEDIO_AUTH)  ;
    }
    public  boolean getHasBidRequest()
    {
       return BitOperationUtil.hasState(bitState, Const.OP_HAS_BIDREQUEST)  ;
    }
    public  void    addState(Long bitState)
    {
        if(!BitOperationUtil.hasState(this.getBitState(),bitState))
        {
            this.setBitState(BitOperationUtil.addState(this.getBitState(),bitState));
        }
    }

    public void removeState(Long bitState)
    {
        if(BitOperationUtil.hasState(this.getBitState(),bitState))
        {
            this.setBitState(BitOperationUtil.removeState(this.getBitState(),bitState));
        }
    }
}
