package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 视频审核对象
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class VedioAuth extends BaseAudit implements Serializable
{

}
