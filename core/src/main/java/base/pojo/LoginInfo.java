package base.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录信息
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class LoginInfo extends BasePojo implements Serializable
{
    private String  username    ;
    private String  password    ;
    private int     state       ;//用户状态
    private int     errorNumber ;//记录错误次数
    private int     userType    ;//记录登录类型
}
