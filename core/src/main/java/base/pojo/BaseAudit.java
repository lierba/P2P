package base.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础的审核对象
 * @author LiuZhi
 */
@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public abstract class BaseAudit extends BasePojo implements Serializable
{
    /**
     * 等待审核
     */
    public static final int STATE_NORMAL    =   0;
    /**
     * 审核通过
     */
    public static final int STATE_AUDIT     =   1;
    /**
     * 审核拒绝
     */
    public static final int STATE_REJECT    =   2;

    protected int           state                ;//审核状态
    protected String          remark               ;//备注
    protected LoginInfo       applier              ;//申请人
    protected LoginInfo       auditor              ;//审核人
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    protected Date applyTime                       ;//申请时间
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    protected Date            auditTime            ;//审核时间
    public String getStateDisplay()
    {
        switch (state)
        {
            case STATE_NORMAL   : return "待审核"  ;
            case STATE_AUDIT    : return "审核通过";
            case STATE_REJECT   : return "审核拒绝";
            default             : return ""        ;
        }
    }
}
