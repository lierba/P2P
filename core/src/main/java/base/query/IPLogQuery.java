package base.query;

import base.pojo.BasePojo;
import common.UserContext;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import util.DateUtil;

import java.util.Date;

@Data
public class IPLogQuery extends BaseQuery
{
    private String  userName   ;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date    beginDate  ;
    private Date    endDate    ;
    private Integer state      ;//状态 NULL 查询全部 0 登录成功的,1 登录失败的

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndDate(Date endDate) {
        this.endDate = DateUtil.endOfDay(endDate);
    }
}
