package base.query;

import lombok.Data;

@Data
public abstract class BaseQuery
{
    protected Integer pageNum ;
    protected Integer pageSize;

    public Integer getPageNum()
    {
        if(pageNum==null || pageNum < 1)
        {
            return 1;
        }
        return pageNum ;
    }

    public Integer getPageSize()
    {
        if(pageSize==null || pageSize < 1)
        {
            return 10;
        }
        return pageSize;
    }
}
