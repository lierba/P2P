package base.query;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import util.DateUtil;

import java.util.Date;
@Data
public class CommonQuery extends BaseQuery
{
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate  ;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date    endDate    ;
    private String  criteria   ;
    private String  state      ;
    private Long    applierId  ;
    public void setEndDate(Date endDate)
    {
        this.endDate = DateUtil.endOfDay(endDate);
    }
    public String getCriteria()
    {
        if(criteria==null || criteria.trim().equals(""))
        {
            return null;
        }
        return this.criteria;
    }
    public String getState()
    {
        if(state==null || state.trim().equals(""))
        {
            return null;
        }
        return this.state;
    }
}
