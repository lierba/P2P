package base.service;

import common.ServerResponse;

/**
 * 手机验证码相关的服务
 * @author LiuZhi
 */
public interface IVerifyCodeService
{
    /**
     * 检查用户是否绑定过手机
     */
    ServerResponse checkBindPhone(String phoneNumber);
    /**
     * 给指定的手机发送验证码
     */
    ServerResponse sendVerifyCode(String phoneNumber) throws Exception;
    /**
     * 绑定手机号码
     */
    ServerResponse bindPhone(String phoneNumber,String verifyCode);
}
