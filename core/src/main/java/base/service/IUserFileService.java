package base.service;

import base.pojo.UserFile;
import base.query.CommonQuery;
import common.ServerResponse;

import java.util.List;

/**
 * 风控资料服务
 */
public interface IUserFileService
{

    void apply(String fileName);

    /**
     * 列出一个用户没有选择风控材料类型的风控资料
     */
    List<UserFile> listUnTypeFiles();

    List<UserFile> listTypeFiles();

    ServerResponse userFileSelectType(Long[] id, Long[] fileType);

    ServerResponse allUserFileAuth(CommonQuery commonQuery);

    ServerResponse getUserFileAuth(Long id);

    ServerResponse UserFileAudit(UserFile userFile) throws Exception;

    List<UserFile> listFilesByAudit(Long id);
}
