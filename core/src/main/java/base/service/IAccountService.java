package base.service;

import base.pojo.Account;
import common.ServerResponse;

/**
 * 账户相关服务
 * @author LiuZhi
 */
public interface IAccountService
{

  /**
   * 更新账户信息
   */
  ServerResponse update (Account account) throws Exception;

  /**
   * 添加账户信息
   */
  ServerResponse add    (Account account) throws Exception;

  Account get(Long id);

  Account getCurrent();
}
