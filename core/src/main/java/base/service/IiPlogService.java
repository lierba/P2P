package base.service;

import base.pojo.IPlog;
import base.query.IPLogQuery;
import com.github.pagehelper.PageInfo;

import java.util.Map;

public interface IiPlogService
{
    /**
     * 保存登录日记
     */
    void            save                (IPlog iPlog)          ;

    /**
     * 获取错误日记
     */
    Map             getErrorLog         (String userName)      ;

    PageInfo<IPlog> getAllIPLogByLimit  (IPLogQuery ipLogQuery);
}
