package base.service;

import base.pojo.UserInfo;
import common.ServerResponse;

/**
 * 用户相关服务
 * @author LiuZhi
 */
public interface IUserInfoService
{
    /**
     * 更新用户信息
     */
    ServerResponse update       (UserInfo userInfo) throws Exception    ;
    /**
     * 添加用户信息
     */
    ServerResponse add          (UserInfo userInfo) throws Exception    ;

    UserInfo       get          (Long id)                               ;

    /**
     * 更新状态码
     */
    void           bindPhone  (Long id,Long state,String phoneNumber)   ;

    int            checkPhone (String phone)                            ;

    void           bindEmail  (Long id,Long state,String email)         ;

    int            checkEmil  (String email)                            ;

    UserInfo       getCurrent ()                                        ;

    ServerResponse updateBasicInfo(UserInfo userInfo)                   ;
}
