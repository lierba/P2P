package base.service.impl;

import base.mapper.SystemDictionaryItemMapper;
import base.mapper.SystemDictionaryMapper;
import base.pojo.SystemDictionary;
import base.pojo.SystemDictionaryItem;
import base.service.ISystemDictionaryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import common.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.PageUtil;

import java.util.List;

@Service
public class SystemDictionaryServiceImpl implements ISystemDictionaryService
{
    @Autowired
    SystemDictionaryItemMapper  systemDictionaryItemMapper  ;
    @Autowired
    SystemDictionaryMapper      systemDictionaryMapper      ;


    public PageInfo<SystemDictionaryItem> allSystemDictionaryItem(Integer pageNum,Integer pageSize,String criteria,Long parentId) {
        PageHelper.startPage(PageUtil.getPageNum(pageNum), PageUtil.getPageSize(pageSize));
        if(criteria ==  null   ||  criteria.trim()  ==  "")
        {
            criteria    =   null;
        }
        List<SystemDictionaryItem> list = systemDictionaryItemMapper.selectAll(criteria,parentId);
        return PageUtil.returnPageInfo(list);
    }

    public PageInfo<SystemDictionary> allSystemDictionary(Integer pageNum, Integer pageSize, String criteria) {
        PageHelper.startPage(PageUtil.getPageNum(pageNum), PageUtil.getPageSize(pageSize));
        List<SystemDictionary> list = systemDictionaryMapper.selectAll(criteria);
        return PageUtil.returnPageInfo(list);
    }

    @Override
    public List<SystemDictionary> allSystemDictionary() {
        return  systemDictionaryMapper.selectAll(null);
    }

    public ServerResponse addOrUpdateSystemDictionary(SystemDictionary systemDictionary) {
        if(systemDictionary.getSn()==null||systemDictionary.getSn().trim().equals(""))
        {
            return ServerResponse.createByErrorMsg("编号不能为空");
        }
        if(systemDictionary.getTitle()==null||systemDictionary.getTitle().trim().equals(""))
        {
            return ServerResponse.createByErrorMsg("字典名称不能为空");
        }
        if(systemDictionary.getId()!=null)
        {
            if(systemDictionaryMapper.updateByPrimaryKey(systemDictionary)  >   0)
            {
                return ServerResponse.createBySuccess() ;
            }
            return ServerResponse.createByError()       ;
        }else
            {
                if(systemDictionaryMapper.insert(systemDictionary)  >   0)
                {
                    return ServerResponse.createBySuccess() ;
                }
                return ServerResponse.createByError()       ;
            }
    }

    public ServerResponse delSystemDictionary(Long id)
    {
        if(systemDictionaryMapper.deleteByPrimaryKey(id)>0)
        {
            return ServerResponse.createBySuccessMsg("删除字典成功");
        }
        return ServerResponse.createByErrorMsg("删除字典失败");
    }

    public ServerResponse getSystemDictionary(Long id)
    {
        SystemDictionary systemDictionary   =   systemDictionaryMapper.selectByPrimaryKey(id);
        if(systemDictionary !=  null)
        {
            return ServerResponse.createBySuccess(systemDictionary);
        }
        return ServerResponse.createByErrorMsg("该ID字典数据不存在");
    }


    public ServerResponse addOrUpdateSystemDictionaryItem(SystemDictionaryItem systemDictionaryItem) {
        if(systemDictionaryItem.getParentId()==null)
        {
            return ServerResponse.createByErrorMsg("字典分类ID不能为空");
        }
        if(systemDictionaryItem.getTitle()==null||systemDictionaryItem.getTitle().trim().equals("")){
            return ServerResponse.createByErrorMsg("字典明细名称不能为空");
        }
        if(systemDictionaryItem.getSequence()<=0){
            return ServerResponse.createByErrorMsg("字典排序值不能小于或等于0");
        }
        if(systemDictionaryItem.getId()!=null)
        {
            if(systemDictionaryItemMapper.updateByPrimaryKey(systemDictionaryItem)  >   0)
            {
                return ServerResponse.createBySuccess() ;
            }
            return ServerResponse.createByError()       ;
        }else
            {
                if(systemDictionaryItemMapper.insert(systemDictionaryItem)  >   0)
                {
                    return ServerResponse.createBySuccess() ;
                }
                return ServerResponse.createByError()       ;
            }
    }

    public ServerResponse getSystemDictionaryItem(Long id)
    {
        SystemDictionaryItem systemDictionaryItem   =   systemDictionaryItemMapper.selectByPrimaryKey(id);
        if(systemDictionaryItem !=  null)
        {
            return ServerResponse.createBySuccess(systemDictionaryItem);
        }
        return ServerResponse.createByErrorMsg("该ID字典数据不存在");
    }

    public ServerResponse delSystemDictionaryItem(Long id)
    {
        if(systemDictionaryItemMapper.deleteByPrimaryKey(id)>0)
        {
            return ServerResponse.createBySuccessMsg("删除字典成功");
        }
        return ServerResponse.createByErrorMsg("删除字典失败");
    }

    @Override
    public List<SystemDictionaryItem> allSystemDictionaryItemBySn(String sn) {
        return systemDictionaryItemMapper.allSystemDictionaryItemBySn(sn);
    }
}
