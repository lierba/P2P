package base.service.impl;

import base.mapper.RealAuthMapper;
import base.mapper.VedioAuthMapper;
import base.pojo.LoginInfo;
import base.pojo.RealAuth;
import base.pojo.UserInfo;
import base.pojo.VedioAuth;
import base.query.CommonQuery;
import base.service.ILoginInfoService;
import base.service.IRealAuthService;
import base.service.IUserInfoService;
import com.github.pagehelper.PageHelper;
import common.Const;
import common.ServerResponse;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.BitOperationUtil;
import util.PageUtil;

import java.util.Date;
import java.util.List;

@Service
public class RealAuthServiceImpl implements IRealAuthService {
    @Autowired
    RealAuthMapper realAuthMapper;
    @Autowired
    IUserInfoService iUserInfoService;
    @Autowired
    VedioAuthMapper vedioAuthMapper;
    @Autowired
    ILoginInfoService iLoginInfoService;

    public RealAuth getRealAuth(Long id) {
        return realAuthMapper.selectByPrimaryKey(id);
    }

    public ServerResponse realApply(RealAuth realAuth) throws Exception {
        UserInfo current = iUserInfoService.getCurrent();
        if (current.getHasRealAuth()) {
            return ServerResponse.createByErrorMsg("您已通过实名认证,请勿重新提交请求");
        }
        if (current.getRealAuthId() != null) {
            return ServerResponse.createByErrorMsg("您的实名认证正在审核中,请勿重新提交请求");
        }
        //保存实名认证对象
        realAuth.setState(RealAuth.STATE_NORMAL);
        realAuth.setApplier(UserContext.getCurrentUser());
        realAuth.setApplyTime(new Date());
        if (realAuthMapper.insert(realAuth) > 0) {
            current.setRealAuthId(realAuth.getId());
            iUserInfoService.update(current);
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByErrorMsg("实名认证请求失败");
    }

    public ServerResponse allRealAuth(CommonQuery commonQuery)
    {
        PageHelper.startPage(commonQuery.getPageNum(),commonQuery.getPageSize())    ;
        List<RealAuth> list = realAuthMapper.selectAll(commonQuery)                 ;
        return ServerResponse.createBySuccess(PageUtil.returnPageInfo(list))        ;
    }

    public RealAuth getRealAuthFull(Long id)
    {
        return realAuthMapper.getFull(id);
    }

    public ServerResponse realAuthAudit(RealAuth realAuth) throws Exception
    {
        RealAuth old = realAuthMapper.getFull(realAuth.getId())                 ;
        old.setState(realAuth.getState())                                       ;
        old.setRemark(realAuth.getRemark())                                     ;
        UserInfo userInfo = iUserInfoService.get(old.getApplier().getId())      ;
        Long bitState = userInfo.getBitState()                                  ;
        if(BitOperationUtil.hasState(bitState,Const.OP_REAL_AUTH))
        {
            return ServerResponse.createByErrorMsg("此用户已实名认证")           ;
        }
        userInfo.setBitState(BitOperationUtil.addState(userInfo.getBitState()
                                                    , Const.OP_REAL_AUTH))      ;
        userInfo.setRealName(old.getRealName())                                 ;
        userInfo.setIdNumber(old.getIdNumber())                                 ;
        old.setAuditor(UserContext.getCurrentUser())                            ;
        old.setAuditTime(new Date())                                            ;
        if(realAuthMapper.updateByPrimaryKey(old,old.getId()) > 0)
        {
            if(iUserInfoService.update(userInfo).getStatus() == 0)
            {
                return ServerResponse.createBySuccess();
            }
        }

        return ServerResponse.createByError();
    }

    public ServerResponse vedioAuthAudit(VedioAuth vedioAuth) throws Exception
    {
        LoginInfo loginInfo = iLoginInfoService.getLoginInfoById(vedioAuth.getId());
        if(loginInfo == null)
        {
            return ServerResponse.createByErrorMsg("此用户不存在")                  ;
        }
        UserInfo  userInfo  = iUserInfoService.get(loginInfo.getId())              ;
        Long bitState = userInfo.getBitState()                                     ;
        if(BitOperationUtil.hasState(bitState,Const.OP_VEDIO_AUTH))
        {
            return ServerResponse.createByErrorMsg("此用户已视频认证")              ;
        }
        vedioAuth.setId(null)                                                      ;
        vedioAuth.setApplier(loginInfo)                                            ;
        vedioAuth.setApplyTime(new Date())                                         ;
        vedioAuth.setAuditor(UserContext.getCurrentUser())                         ;
        vedioAuth.setAuditTime(new Date())                                         ;
        userInfo.setBitState(BitOperationUtil.addState(userInfo.getBitState()
                , Const.OP_VEDIO_AUTH))                                            ;
        if(vedioAuthMapper.insert(vedioAuth) > 0)
        {
            if(vedioAuth.getId() == null)
            {
                return ServerResponse.createByErrorMsg("数据库返回的ID为空");
            }
            userInfo.setVedioAuthId(vedioAuth.getId())  ;
            if(iUserInfoService.update(userInfo).getStatus() == 0)
            {
                return ServerResponse.createBySuccess() ;
            }
        }
        return ServerResponse.createByError()           ;
    }

    public VedioAuth getVedioAuth(Long id)
    {
        return vedioAuthMapper.selectByPrimaryKey(id);
    }

    public ServerResponse allVedioAuth(CommonQuery commonQuery)
    {
        PageHelper.startPage(commonQuery.getPageNum(),commonQuery.getPageSize())    ;
        List<VedioAuth> list = vedioAuthMapper.selectAll(commonQuery)               ;
        return ServerResponse.createBySuccess(PageUtil.returnPageInfo(list))        ;
    }

    public ServerResponse autoCompletion(String text)
    {
        return ServerResponse.createBySuccess(iLoginInfoService.getIdByDimUsername(text));
    }

}