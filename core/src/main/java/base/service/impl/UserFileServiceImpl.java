package base.service.impl;

import base.mapper.UserFileMapper;
import base.pojo.SystemDictionaryItem;
import base.pojo.UserFile;
import base.pojo.UserInfo;
import base.query.CommonQuery;
import base.service.IUserFileService;
import base.service.IUserInfoService;
import com.github.pagehelper.PageHelper;
import common.ServerResponse;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.PageUtil;

import java.util.Date;
import java.util.List;

@Service
public class UserFileServiceImpl implements IUserFileService
{
    @Autowired
    UserFileMapper      userFileMapper  ;
    @Autowired
    IUserInfoService    iUserInfoService;

    public void apply(String fileName)
    {
        UserFile userFile = new UserFile()                  ;
        userFile.setApplier(UserContext.getCurrentUser())   ;
        userFile.setApplyTime(new Date())                   ;
        userFile.setState(UserFile.STATE_NORMAL)            ;
        userFile.setImage(fileName)                         ;
        userFileMapper.insert(userFile)                     ;
    }

    public List<UserFile> listUnTypeFiles() {
        return userFileMapper.listUnTypeFiles(UserContext.getCurrentUser().getId());
    }

    @Override
    public List<UserFile> listTypeFiles()
    {
        return userFileMapper.listTypeFiles(UserContext.getCurrentUser().getId());
    }

    public ServerResponse userFileSelectType(Long[] id, Long[] fileType)
    {
        if(id != null && id.length == fileType.length)
        {
            for(int i=0;i<id.length;i++)
            {
                UserFile uf = userFileMapper.selectByPrimaryKey(id[i])  ;
                SystemDictionaryItem sd = new SystemDictionaryItem()    ;
                sd.setId(fileType[i])                                   ;
                uf.setFileTypes(sd)                                     ;
                if(userFileMapper.updateByPrimaryKey(uf) <= 0)
                {
                    ServerResponse.createBySuccessMsg("更新认证信息失败");
                }
            }
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createBySuccessMsg("没有选择任何认证信息") ;
    }

    public ServerResponse allUserFileAuth(CommonQuery commonQuery)
    {
        PageHelper.startPage(commonQuery.getPageNum(),commonQuery.getPageSize())    ;
        List<UserFile> list = userFileMapper.selectAll(commonQuery)                 ;
        return ServerResponse.createBySuccess(PageUtil.returnPageInfo(list))        ;
    }

    public ServerResponse getUserFileAuth(Long id)
    {
        return ServerResponse.createBySuccess(userFileMapper.selectByPrimaryKey(id));
    }

    public ServerResponse UserFileAudit(UserFile userFile) throws Exception
    {
        UserFile old = userFileMapper.selectByPrimaryKey(userFile.getId())      ;
        old.setState(userFile.getState())                                       ;
        old.setRemark(userFile.getRemark())                                     ;
        old.setAuditor(UserContext.getCurrentUser())                            ;
        old.setAuditTime(new Date())                                            ;
        if(userFile.getState()==1)
        {
            old.setScore(userFile.getScore())                                   ;
            UserInfo userInfo = iUserInfoService.get(old.getApplier().getId())  ;
            int score = userInfo.getScore()+userFile.getScore()                 ;
            userInfo.setScore(score)                                            ;
            if(userFileMapper.updateByPrimaryKey(old) > 0)
            {
                if(iUserInfoService.update(userInfo).getStatus() == 0)
                {
                    return ServerResponse.createBySuccess()                     ;
                }
            }
        }else
            {
                if(userFileMapper.updateByPrimaryKey(old) > 0)
                {
                        return ServerResponse.createBySuccess()                 ;
                }
            }

        return ServerResponse.createByError()                                   ;
    }

    public List<UserFile> listFilesByAudit(Long id)
    {
        return userFileMapper.listFilesByAudit(id);
    }
}

