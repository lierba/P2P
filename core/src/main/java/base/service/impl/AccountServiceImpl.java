package base.service.impl;

import base.mapper.AccountMapper;
import base.pojo.Account;
import base.service.IAccountService;
import common.ServerResponse;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 账户相关服务
 * @author LiuZhi
 */
@Service
public class AccountServiceImpl implements IAccountService
{
    @Autowired
    AccountMapper accountMapper;

    public ServerResponse update(Account account) throws Exception
    {
        int ret  =   accountMapper.updateByPrimaryKey(account);
        if( ret  <=  0)
        {
            throw new RuntimeException("乐观锁失败,Account:"+account.getId()+"--Version:"+account.getVersion());
        }
        return ServerResponse.createBySuccess();
    }

    public ServerResponse add(Account account) throws Exception
    {
        int ret =   accountMapper.insert(account)            ;
        if( ret <=  0 )
        {
            throw new RuntimeException("创建用户详情信息错误");
        }
        return ServerResponse.createBySuccess()              ;
    }

    public Account get(Long id)
    {
        return accountMapper.selectByPrimaryKey(id);
    }

    public Account getCurrent()
    {
        return get(UserContext.getCurrentUser().getId());
    }

}
