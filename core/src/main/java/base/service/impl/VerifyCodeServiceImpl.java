package base.service.impl;

import base.pojo.UserInfo;
import base.service.IUserInfoService;
import base.service.IVerifyCodeService;
import base.vo.VerifyCodeVo;
import com.aliyuncs.exceptions.ClientException;
import common.Const;
import common.SMSGateway;
import common.ServerResponse;
import common.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.BitOperationUtil;
import util.DateUtil;
import java.util.Date;
import java.util.UUID;

@Service
public class VerifyCodeServiceImpl implements IVerifyCodeService
{
    @Autowired
    IUserInfoService iUserInfoService;

    @Override
    public ServerResponse checkBindPhone(String phoneNumber) {
        UserInfo userInfo = iUserInfoService.get(UserContext.getCurrentUser().getId());
        if(userInfo.getHasBindPhone())
        {
            return ServerResponse.createByErrorMsg("您已绑定过手机,请勿重复绑定")         ;
        }
        if(iUserInfoService.checkPhone(phoneNumber)>0)
        {
            return ServerResponse.createByErrorMsg("该手机号码已被绑定,请更换手机号码绑定");
        }
        return ServerResponse.createBySuccess()                                          ;
    }

    public ServerResponse sendVerifyCode(String phoneNumber) throws Exception {
        ServerResponse sr = checkBindPhone(phoneNumber);
        if(sr.getStatus() == 1)
        {
            return sr                                  ;
        }
        if(UserContext.getVerifyCodeVo()!=null)
        {
            long timeDifference= DateUtil.secondsBetween(new Date(),UserContext.getVerifyCodeVo().getLastSendTime());
            if(timeDifference<60)
            {
                return ServerResponse.createByErrorMsg("发送过于频繁");
            }
        }
        String randomCode    =  UUID.randomUUID().toString().substring(0,4);
        SMSGateway smsGateway=  new SMSGateway()              ;
        sr                   =  smsGateway.sendSMS(phoneNumber,
                                                   randomCode);
        if(sr.getStatus()   ==  1)
        {
            return sr                                         ;
        }
        VerifyCodeVo codeVo  =  new VerifyCodeVo()            ;
        codeVo.setLastSendTime(new Date())                    ;
        if(phoneNumber==null)
        {
            return ServerResponse.createByErrorMsg("参数错误");
        }
        codeVo.setPhoneNumber(phoneNumber)                    ;
        codeVo.setVerifyCode (randomCode)                     ;
        UserContext.putVerifyCodeVo(codeVo)                   ;
        return ServerResponse.createBySuccess("发送验证码成功");
    }

    public ServerResponse bindPhone(String phoneNumber, String verifyCode)
    {
        if(UserContext.getVerifyCodeVo()!=null)
        {
            long timeDifference= DateUtil.secondsBetween(new Date(),UserContext.getVerifyCodeVo().getLastSendTime());
            if(timeDifference>600)
            {
                return ServerResponse.createByErrorMsg("验证码已过期")                                ;
            }
            VerifyCodeVo vo = UserContext.getVerifyCodeVo()                                          ;
            if(!phoneNumber.equals(vo.getPhoneNumber()))
            {
                return ServerResponse.createByErrorMsg("验证码与绑定的手机号码不匹配")                ;
            }
            if(!vo.getVerifyCode().equals(verifyCode))
            {
                return ServerResponse.createByErrorMsg("验证码有误")                                 ;
            }
            UserInfo userInfo = iUserInfoService.get(UserContext.getCurrentUser().getId())           ;
            Long     states   = BitOperationUtil.addState(userInfo.getBitState(),Const.OP_BIND_PHONE);
            iUserInfoService.bindPhone(UserContext.getCurrentUser().getId(),states,phoneNumber)      ;
            return ServerResponse.createBySuccess("绑定手机成功")                                     ;
        }
        return ServerResponse.createByErrorMsg("验证码不存在或者已过期")                              ;
    }

}
