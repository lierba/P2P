package base.service;

import base.pojo.RealAuth;
import base.pojo.VedioAuth;
import base.query.CommonQuery;
import common.ServerResponse;

/**
 * 实名认证对象服务
 */
public interface IRealAuthService
{
    RealAuth getRealAuth(Long id);

    /**
     * 实名认证申请
     */
    ServerResponse realApply        (RealAuth realAuth) throws Exception;

    ServerResponse allRealAuth  (CommonQuery commonQuery)           ;

    RealAuth       getRealAuthFull      (Long id)                           ;

    ServerResponse realAuthAudit(RealAuth realAuth) throws Exception;

    ServerResponse vedioAuthAudit(VedioAuth vedioAuth)throws Exception;

    VedioAuth      getVedioAuth(Long id);

    ServerResponse allVedioAuth(CommonQuery commonQuery);

    ServerResponse autoCompletion(String text   );
}
