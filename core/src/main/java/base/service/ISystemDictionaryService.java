package base.service;

import base.pojo.SystemDictionary;
import base.pojo.SystemDictionaryItem;
import com.github.pagehelper.PageInfo;
import common.ServerResponse;

import java.util.List;


public interface ISystemDictionaryService
{
    PageInfo<SystemDictionaryItem>  allSystemDictionaryItem(Integer pageNum,Integer pageSize,
                                                                                String criteria,Long parentId)  ;

    PageInfo<SystemDictionary>      allSystemDictionary(Integer pageNum,Integer pageSize,String criteria)       ;

    List<SystemDictionary>          allSystemDictionary()                                                       ;

    ServerResponse                  addOrUpdateSystemDictionary(SystemDictionary systemDictionary)              ;

    ServerResponse                  delSystemDictionary(Long id)                                                ;

    ServerResponse                  getSystemDictionary(Long id)                                                ;

    ServerResponse                  addOrUpdateSystemDictionaryItem(SystemDictionaryItem systemDictionaryItem)  ;

    ServerResponse                  getSystemDictionaryItem(Long id)                                            ;

    ServerResponse                  delSystemDictionaryItem(Long id)                                            ;

    /**
     * 根据数据字典分类SN查询明细
     */
    List<SystemDictionaryItem>      allSystemDictionaryItemBySn(String sn)                                         ;
}
