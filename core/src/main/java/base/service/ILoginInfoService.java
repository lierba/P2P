package base.service;


import base.pojo.LoginInfo;
import com.github.pagehelper.PageInfo;
import common.ServerResponse;

import java.util.List;

/**
 * 登录/注册相关服务
 * @author LiuZhi
 */
public interface ILoginInfoService
{

    /**
     * 用户注册接口
     */
    ServerResponse  register        (String username, String password) throws Exception ;

    /**
     * 查询用户是否存在
     */
    ServerResponse  checkUsername   (String username)                                   ;
    /**
     * 验证登录
     */
    ServerResponse  login           (String username    ,
                                     String password    ,
                                     String remoteAddr  ,int userType)                  ;
    /**
     * 设置错误数量为0
     */
    void updateErrorNumber          (Long id)                                           ;
    /**
     * 修改错误数量+1
     */
    void addErrorNumber             (String username)                                   ;
    /**
     * 检查是否存在管理员
     */
    void checkAdmin                 ()                                                  ;
    LoginInfo   getLoginInfoById(Long id)                                               ;

    PageInfo getIdByDimUsername(String text)                                            ;
}
