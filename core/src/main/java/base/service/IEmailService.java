package base.service;

import base.pojo.MailVerify;
import common.ServerResponse;

import javax.mail.MessagingException;

public interface IEmailService
{
    /**
     * 校验邮箱
     */
    ServerResponse  checkEmail  (String email);

    /**
     * 发送邮件
     */
    ServerResponse  sendEmail   (String email) throws Exception;
    /**
     * 绑定邮箱
     */
    ServerResponse  bindEmail   (String checkCode);
}
