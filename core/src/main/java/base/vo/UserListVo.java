package base.vo;

import base.pojo.BasePojo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserListVo extends BasePojo implements Serializable
{
    private String  username    ;
    private int     state       ;
    private Integer sex         ;
    private String  phoneNumber ;
    private String  email       ;
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd hh:mm:ss")
    private Date    createTime  ;
    private String  idNumber    ;
    private String  address     ;

    public String getSexDisplay()
    {
        if(sex == null){
            return null;
        }
        return sex== 0? "男" : "女" ;
    }
}
