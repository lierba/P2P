package base.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 存放验证码相关内容
 */
@Data
public class VerifyCodeVo implements Serializable
{
    private String  verifyCode  ;//验证码
    private String  phoneNumber ;//发送验证码的手机号
    private Date    lastSendTime;//最后一次成功发送的时间
}
