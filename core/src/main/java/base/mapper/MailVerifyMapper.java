package base.mapper;

import base.pojo.MailVerify;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MailVerifyMapper {
    int                 deleteByPrimaryKey  (Long id)                       ;

    int                 insert              (MailVerify record)             ;

    MailVerify          selectByPrimaryKey  (Long id)                       ;

    List<MailVerify>    selectAll           ()                              ;

    int                 updateByPrimaryKey  (MailVerify record)             ;

    MailVerify          getEmailByEmail     (String email)                  ;

    MailVerify          getByRandomCode     (String randomCode)             ;

}