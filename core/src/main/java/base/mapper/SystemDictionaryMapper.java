package base.mapper;

import java.util.List;

import base.pojo.SystemDictionary;
public interface SystemDictionaryMapper
{
    int                     deleteByPrimaryKey  (Long id)                   ;

    int                     insert              (SystemDictionary record)   ;

    SystemDictionary        selectByPrimaryKey  (Long id)                   ;

    List<SystemDictionary>  selectAll           (String criteria)           ;

    int                     updateByPrimaryKey  (SystemDictionary record)   ;
}