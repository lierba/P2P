package base.mapper;
import base.pojo.UserInfo;
import base.query.CommonQuery;
import base.vo.UserListVo;
import common.ServerResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper {
    int                 deleteByPrimaryKey  (Long id)                       ;

    int                 insert              (UserInfo record)               ;

    UserInfo            selectByPrimaryKey  (Long id)                       ;

    List<UserInfo>      selectAll           ()                              ;

    int                 updateByPrimaryKey  (UserInfo record)               ;

    void                updateState(@Param("id") Long id
                                ,@Param("state") Long state
                                ,@Param("phone") String phone)              ;
    int                 checkPhone          (String phone)                  ;

    int                 bindEmail           (@Param("id")    Long   id      ,
                                             @Param("state") Long   state   ,
                                             @Param("email") String email)  ;

    int                 checkEmail          (String email)                  ;

    List<UserListVo>    allUser             (CommonQuery commonQuery)       ;
}