package base.mapper;

import base.pojo.RealAuth;
import base.query.CommonQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RealAuthMapper
{
    int             deleteByPrimaryKey(Long id)             ;

    int             insert(RealAuth record)                 ;

    RealAuth        selectByPrimaryKey(Long id)             ;

    List<RealAuth>  selectAll(CommonQuery commonQuery)  ;

    int             updateByPrimaryKey(@Param("realAuth") RealAuth record,@Param("id") Long id)     ;

    RealAuth        getFull(Long id)                        ;
}