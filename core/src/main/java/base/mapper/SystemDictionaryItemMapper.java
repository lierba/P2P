package base.mapper;

import java.util.List;

import base.pojo.SystemDictionaryItem;
import org.apache.ibatis.annotations.Param;

public interface SystemDictionaryItemMapper
{

    int                         deleteByPrimaryKey  (Long id)                           ;

    int                         insert              (SystemDictionaryItem record)       ;

    SystemDictionaryItem        selectByPrimaryKey  (Long id)                           ;

    List<SystemDictionaryItem>  selectAll           (@Param("criteria")String criteria  ,
                                                     @Param("parentId")Long   parentId) ;

    int                         updateByPrimaryKey  (SystemDictionaryItem record)       ;

    List<SystemDictionaryItem>  allSystemDictionaryItemBySn(String sn)                  ;
}