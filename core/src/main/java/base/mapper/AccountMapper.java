package base.mapper;

import java.util.List;

import base.pojo.Account;

public interface AccountMapper
{
    int             deleteByPrimaryKey  (Long id)       ;

    int             insert              (Account record);

    Account         selectByPrimaryKey  (Long id)       ;

    List<Account>   selectAll           ()              ;

    int             updateByPrimaryKey  (Account record);
}