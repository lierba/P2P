package base.mapper;

import base.pojo.VedioAuth;
import base.query.CommonQuery;

import java.util.List;

public interface VedioAuthMapper
{
    int             deleteByPrimaryKey  (Long id)                       ;

    int             insert              (VedioAuth record)              ;

    VedioAuth       selectByPrimaryKey  (Long id)                       ;

    List<VedioAuth> selectAll           (CommonQuery commonQuery)       ;

    int             updateByPrimaryKey  (VedioAuth record)              ;
}