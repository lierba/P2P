package base.mapper;

import java.util.List;
import base.pojo.LoginInfo;
import org.apache.ibatis.annotations.Param;

public interface LoginInfoMapper
{

    /**
     * 添加一个登录用户信息
     */
    int             insert              (LoginInfo record)                       ;

    /**
     * 根据主键查询登录用户信息
     */
    LoginInfo       selectByPrimaryKey  (Long id)                               ;

    /**
     * 查询所有登录用户信息
     */
    List<LoginInfo> selectAll           ()                                      ;

    /**
     * 根据登录用户主键更新用户信息
     */
    int             updateByPrimaryKey  (LoginInfo record)                      ;

    /**
     * 根据用户名查询用户数量
     */
    int             getCountByUserName  (String name)                           ;

    /**
     * 用户登录
     */
    LoginInfo       login               (@Param("username") String username     ,
                                         @Param("password") String password     ,
                                         @Param("userType") int    userType)    ;

    /**
     * 设置错误数量为0
     */
    void            updateErrorNumber  (Long id)                                ;

    void            addErrorNumber     (String username)                        ;

    int             getCountByUserType (int userManage)                         ;

    List<LoginInfo> getIdByDimUsername  (@Param("text") String text)            ;
}