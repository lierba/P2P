package base.mapper;

import base.pojo.UserFile;
import base.query.CommonQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserFileMapper {

    int deleteByPrimaryKey(Long id);

    int insert(UserFile record);

    UserFile selectByPrimaryKey(Long id);

    List<UserFile> selectAll(CommonQuery commonQuery);

    int updateByPrimaryKey(UserFile record);

    List<UserFile> listUnTypeFiles(@Param("id") Long id);

    List<UserFile> listTypeFiles(@Param("id") Long id);

    List<UserFile> listFilesByAudit(@Param("id") Long id);
}