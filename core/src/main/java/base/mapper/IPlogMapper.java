package base.mapper;

import base.pojo.IPlog;
import base.query.IPLogQuery;

import java.util.List;
import java.util.Map;

public interface IPlogMapper {
    int deleteByPrimaryKey      (Long id)               ;

    int insert                  (IPlog record)          ;

    IPlog selectByPrimaryKey    (Long id)               ;

    List<IPlog> selectAll       ()                      ;

    List<IPlog> selectAllById   (IPLogQuery ipLogQuery) ;

    int updateByPrimaryKey      (IPlog record)          ;

    Map getErrorLog             (String username)       ;
}